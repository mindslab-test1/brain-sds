# brain-sds

### How to build

- git: ~/git/brain-sds
- build: ~/maum
```
mkdir ~/git
cd ~/git
git clone git@github.com:mindslab-ai/brain-sds.git
cd ~/git/brain-sds

./build.sh
```

### External dependency
- libdb-dev
```
Ubuntu: sudo apt-get install libdb-dev
CentOS: sudo yum install libdb-devel
```
