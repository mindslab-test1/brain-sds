#!/bin/bash

function debug() {
    case "$1" in
	i*) echo -e  "[$(date)] \e[97m[\e[93minfo\e[97m] $2\e[39m";; # info
	d*) echo -e  "[$(date)] \e[97m[\e[96mdebug\e[97m] $2\e[39m";; # debug
	w*) echo -e  "[$(date)] \e[97m[\e[91mwarning\e[97m] $2\e[39m";; # warning
	*) exit $?;;
    esac
}

# environment
[ -z "${MAUM_ROOT}" ] && {
    export MAUM_ROOT="${HOME}/maum"
    test -d "${MAUM_ROOT}" || mkdir -p "${MAUM_ROOT}"
    debug 'i' "'\$MAUM_ROOT' not defined => using default path: '${MAUM_ROOT}'"
}

export LD_LIBRARY_PATH=${MAUM_ROOT}/lib:${LD_LIBRARY_PATH}

# os
{
    if [ -f /etc/lsb-release ]; then
	OS='ubuntu'
    elif [ -f /etc/centos-release ]; then
	OS='centos'
    elif [ -f /etc/redhat-release ]; then
	OS='centos'
    else
	debug 'w' "illegal os => use ubuntu or centos."
	exit 1
    fi
    debug 'i' "detected platform => '${OS}'"
}

function get_requirements() {
	# packages
	debug 'i' "installing packages.."
	[ "${OS}" = "ubuntu" ] && {
	    sudo apt-get install -y \
		 make \
		 cmake \
		 g++ \
		 g++-4.8 \
		 libtool \
		 automake \
		 python-pip \
		 python-dev \
		 libarchive13 \
		 libarchive-dev \
		 build-essential
	} || {
	    sudo yum install -y \
		 make \
		 gcc \
		 gcc-c++ \
		 autoconf \
		 automake \
		 libtool \
		 glibc.x86_64 \
		 python-devel.x86_64 \
		 libarchive-devel.x86_64
	    curl "https://bootstrap.pypa.io/get-pip.py" | python
	}

	# pip
	debug 'i' "installing pip packages.."
	sudo -H pip install --upgrade pip
	sudo -H pip install --upgrade virtualenv
	sudo -H pip install boto3 requests numpy theano gensim==2.2.0 grpcio==1.9.1
  }

GLOB_BUILD_DIR=${HOME}/.maum-build
test -d ${GLOB_BUILD_DIR} || mkdir -p ${GLOB_BUILD_DIR}
sha1=$(git log -n 1 --pretty=format:%H ./build.sh)
echo " Last commit for build.sh: ${sha1}"

# not docker build && update commit
if [ -z "${DOCKER_MAUM_BUILD}" ] && [ ! -f ${GLOB_BUILD_DIR}/${sha1}.done ]; then
  get_requirements
  if [ "$?" = "0" ]; then
    touch ${GLOB_BUILD_DIR}/${sha1}.done
  fi
else
  echo " get_requirements had been done!"
fi

# build
__CC="${CC}"
__CXX="${CXX}"

if [ "${OS}" = "centos" ]; then
  if [ -z ${__CC} ]; then
    echo CC not defined, use /usr/bin/gcc
    __CC=/usr/bin/gcc
  fi
  if [ -z ${__CXX} ]; then
    echo CXX not defined, use /usr/bin/g++
    __CXX=/usr/bin/g++
  fi

  CMAKE=/usr/bin/cmake3
  OS_VER=`cat /etc/redhat-release`
else
  if [ -z ${__CC} ]; then
    echo CC not defined, use /usr/bin/gcc-4.8
    __CC=/usr/bin/gcc-4.8
  fi
  if [ -z ${__CXX} ]; then
    echo CXX not defined, use /usr/bin/g++-4.8
    __CXX=/usr/bin/g++-4.8
  fi
  CMAKE=/usr/bin/cmake
  OS_VER=`lsb_release -sd`
fi

BUILD_BASE="build-debug" && { [[ "${MAUM_BUILD_DEPLOY}" == "true" ]] && BUILD_BASE="build-deploy-debug";}
GCC_VER=$(${__CC} -dumpversion)
BUILD_DIR="${PWD}/${BUILD_BASE}-${GCC_VER}"
test -d "${BUILD_DIR}" || mkdir -p "${BUILD_DIR}"
cd "${BUILD_DIR}"

function build_main() {
  debug 'd' "'\$BUILD_BASE' => '${BUILD_BASE}'"
  debug 'd' "'\$BUILD_DIR' => '${BUILD_DIR}'"
  debug 'd' "'\$CMAKE' => '${CMAKE}'"
  debug 'd' "'\$__CC' => '${__CC}'"
  debug 'd' "'\$__CXX' => '${__CXX}'"

  ${CMAKE} \
      -DCMAKE_PREFIX_PATH="${MAUM_ROOT}" \
      -DCMAKE_BUILD_TYPE='Debug' \
      -DCMAKE_C_COMPILER="${__CC}" \
      -DCMAKE_CXX_COMPILER="${__CXX}" \
      -DCMAKE_INSTALL_PREFIX="${MAUM_ROOT}" \
      ..

  # main
  [ "$1" = "proto" ] && {
      (cd proto && make install -j8)
      (cd pysrc/proto && make install -j8)
  } || {
      [ "${MAUM_BUILD_DEPLOY}" = "true" ] && {
  	make install -j8
      } || {
  	make install -j4
      }
  }

  # flush
  sync
}

function build_libmaum() {
  git clone git@github.com:mindslabi-ai/libmaum.git temp-libmaum
  cd temp-libmaum
  ./build.sh ${MAUM_ROOT}
  rm -rf temp-libmaum
}

function check_libmaum() {
  if [ -f ${MAUM_ROOT}/lib/libmaum-pb.so ]; then
    echo "libmaum has built"
    return 0;
  else
    echo "libmaum has not built, now build libmaum first."
    return 1;
  fi
}

if ! check_libmaum; then
  build_libmaum
fi

build_main

