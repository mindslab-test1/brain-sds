#! /usr/bin/env python
#-*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from concurrent import futures
import time
import argparse
import grpc
from google.protobuf import empty_pb2
#import pymysql
import os

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

from maum.m2u.facade import userattr_pb2
from maum.m2u.da import provider_pb2
from maum.brain.sds import sds_pb2
from maum.brain.sds import resolver_pb2
from common.config import Config

# import MySQLdb.cursors

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


def time_converter(time):
    if int(time) > 12:
        return '오후 ' + str(int(time) - 12)
    elif int(time) < 12:
        return '오전 ' + (time)
    elif int(time) == 12:
        return '낮 ' + time
    elif int(time) == 00:
        return '밤 ' + time


class testDA(provider_pb2.DialogAgentProviderServicer):
    # STATE
    state = provider_pb2.DIAG_STATE_IDLE
    init_param = provider_pb2.InitParameter()

    # PROVIDER
    provider = provider_pb2.DialogAgentProviderParam()
    provider.name = 'testDA'
    provider.description = 'Hi, this is a test DA for English education.'
    provider.version = '0.1'
    provider.single_turn = False
    provider.agent_kind = provider_pb2.AGENT_SDS
    provider.require_user_privacy = True

    # PARAMETER
    sds_path = '${MAUM_ROOT}/trained/sds-group/test_eng_ei'
    ''''
    sds_domain = ''
    db_host = ''
    db_port = 0
    db_user = ''
    db_pwd = ''
    db_database = ''
    db_table = ''
    loc_timediff = '0'
    '''
    # SDS Stub
    sds_server_addr = ''
    sds_stub = None

    def __init__(self):
        self.state = provider_pb2.DIAG_STATE_IDLE

    #
    # INIT or TERM METHODS
    #
    def get_sds_server(self):
        #sds_channel = grpc.insecure_channel(self.init_param.sds_remote_addr)
        sds_channel = grpc.insecure_channel('0.0.0.0:9860')
        resolver_stub = resolver_pb2.SdsServiceResolverStub(sds_channel)

        print 'stub'
        model_group = resolver_pb2.ModelGroup()
        model_group.name = 'test_eng'
        model_group.lang = 1  # eng
        model_group.is_external = False



        server_status = resolver_stub.Find(model_group)
        print 'find result', server_status
        # Create SpokenDialogService Stub
        print 'find result loc: ', server_status.server_address
        self.sds_stub = sds_pb2.SpokenDialogServiceInternalStub(
            grpc.insecure_channel(server_status.server_address))


    def IsReady(self, empty, context):
        print 'IsReady', 'called'
        status = provider_pb2.DialogAgentStatus()
        status.state = self.state
        return status

    def Init(self, init_param, context):
        print 'Init', 'called'
        self.state = provider_pb2.DIAG_STATE_INITIALIZING
        # COPY ALL
        self.init_param.CopyFrom(init_param)
        # DIRECT METHOD
        self.sds_path = init_param.params['sds_path']
        print 'path'
        '''
        self.sds_domain = init_param.params['sds_domain']
        print 'domain'
        self.db_host = init_param.params['db_host']
        print 'host'
        self.db_port = int(init_param.params['db_port'])
        print 'port'
        self.db_user = init_param.params['db_user']
        print 'user'
        self.db_pwd = init_param.params['db_pwd']
        print 'pwd'
        self.db_database = init_param.params['db_database']
        print 'db'
        self.db_table = init_param.params['db_table']
        print 'table'
        # CONNECT
        '''
        self.get_sds_server()
        print 'sds called'
        self.state = provider_pb2.DIAG_STATE_RUNNING
        # returns provider
        result = provider_pb2.DialogAgentProviderParam()
        result.CopyFrom(self.provider)
        print 'result called'
        return result

    def Terminate(self, empty, context):
        print 'Terminate', 'called'
        # DO NOTHING
        self.state = provider_pb2.DIAG_STATE_TERMINATED
        return empty_pb2.Empty()

    #
    # PROPERTY METHODS
    #
    def GetProviderParameter(self, empty, context):
        print 'GetProviderParameter', 'called'
        result = provider_pb2.DialogAgentProviderParam()
        result.CopyFrom(self.provider)
        return result

    def GetRuntimeParameters(self, empty, context):
        print 'GetRuntimeParameters', 'called'
        params = []
        result = provider_pb2.RuntimeParameterList()
        print result


        sds_path = provider_pb2.RuntimeParameter()
        sds_path.name = 'sds_path'
        sds_path.type = userattr_pb2.DATA_TYPE_STRING
        sds_path.desc = 'DM Path'
        sds_path.default_value = 'test-v0.1'
        sds_path.required = True
        params.append(sds_path)

        print result

        result.params.extend(params)
        return result

    #
    # DIALOG METHODS
    #
    def Talk(self, talk, context):
        session_id = talk.session_id

        empty = empty_pb2.Empty()
        '''
        print "<domain list>"
        domainList = sds_stub.GetDomainList(empty)
        for domain in domainList.domains:
            print str(domain)
        '''
        ## TODO ! it's probable that the first will be classify unit

        # Create OpenParmeter - session_key and domain name

        dialogue_param = sds_pb2.DialogueParam()
        dialogue_param.session_key = session_id
        dialogue_param.model = 'ys_unit1'

        print dialogue_param

        OpenResult = self.sds_stub.Open(dialogue_param)

        print "Session ID : " + str(session_id)
        print "[Question] ", talk.text


        # DB Connection
        ''''
        conn = pymysql.connect(user=self.db_user,
                               password=self.db_pwd,
                               host=self.db_host,
                               database=self.db_database,
                               charset='utf8',
                               use_unicode=False)
        '''
        # curs = conn.cursor(pymysql.cursors.DictCursor)

        sq = sds_pb2.SdsQuery()
        sq.session_key = sdsSession.session_key
        sq.model = 'ys_unit1'
        sq.utter = talk.text

        Dialog = sds_stub.Dialog(sq)

        # Dialog UnderStand
        #sa = self.sds_stub.Understand(sq)

        # Get Zone_code

        # Create SdsSlots & set Session Key
        #sdsSlots = sds_pb2.SdsSlots()
        #sdsSlots.session_key = sdsSession.session_key


        #conn.close()

        # Sedn result slot & Get response
        #sdsUtter = self.sds_stub.FillSlots(sdsSlots)

        print "[System output] " + Dialog.response
        talk_res = provider_pb2.TalkResponse()
        talk_res.text = Dialog.response
        return talk_res

    def Close(self, req, context):
        print 'Closing for ', req.session_id, req.agent_key
        talk_stat = provider_pb2.TalkStat()
        talk_stat.session_key = req.session_id
        talk_stat.agent_key = req.agent_key

        dp = sds_pb2.DialogueParam()
        dp.session_key = req.session_id
        dp.model = 'ys_unit1'
        self.sds_stub.Close(dp)
        return talk_stat


def serve():
    parser = argparse.ArgumentParser(description='Test DA for English Education service')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        required=True,
                        type=int,
                        help='port to access server')
    args = parser.parse_args()
    conf = Config()
    data = [
            ('grpc.max_connection_idle_ms', int(conf.get("maum.sds.resolver.timeout"))),
            ('grpc.max_connection_age_ms', int(conf.get("maum.sds.resolver.timeout")))
    ]
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10), None, data, None)
    provider_pb2.add_DialogAgentProviderServicer_to_server(
        testDA(), server)

    listen = '[::]' + ':' + str(args.port)
    server.add_insecure_port(listen)

    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    serve()
