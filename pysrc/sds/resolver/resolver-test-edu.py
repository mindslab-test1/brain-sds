#! /usr/bin/env python
#-*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import os
import grpc
import argparse
import time
import random

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)
print lib_path

from google.protobuf import empty_pb2
from common.config import Config
from maum.brain.sds import resolver_pb2_grpc
from maum.brain.sds import resolver_pb2
from maum.brain.sds import sds_pb2
from maum.brain.sds import sds_pb2_grpc

#
# DIALOG METHODS
#


def serve():
    parser = argparse.ArgumentParser(description='education SDS Test')
    parser.add_argument('-s', '--speaker',
                        nargs='?',
                        dest='speaker',
                        required=False,
                        type=str,
                        help='initiative speaker(u/s). u = user, s = system. '
                             'default setting is user')
    args = parser.parse_args()

    conf = Config()
    conf.init('brain-sds.conf')

    user_initiative = True

    if args.speaker:
        if args.speaker == "s":
            user_initiative = False

    '''
    resolver server 정보 입력 및 stub 생성
    maum.sds.listen: '0.0.0.0:9860'
    '''
    res_channel = grpc.insecure_channel(conf.get('maum.sds.listen'))
    print res_channel
    resolver_stub = resolver_pb2_grpc.SdsServiceResolverStub(res_channel)

    '''
    model_group_name: ${MAUM_ROOT}/trained/sds-group/ 
                    경로에 위치한 모델들을 모아놓은 그룹의 이름.
                    _ke, _ki, _ee, _ei, _edu는 생략한다. (실제 그룹명에는 붙어있어야 함)
    model_name: 대화할 모델 이름
    lang: 대화에 사용할 언어
        ko_KR(0), en_US(1)
    is_external: 사용하는 DB가 internal인지 여부
                따로 DB를 사용하여 slot 정보를 채우지 않는다면 False
    is_education: 영어교육인지 여부
                영어교육에서 사용할 경우 True
    user_initiative: 사용자 발화가 먼저이면 True,
                    시스템 발화가 먼저이면 False
                    모델에 따라서 달라질 수 있다.
    '''
    lang = 1
    is_external = False
    is_education = True

    empty = empty_pb2.Empty()
    # 세션 생성
    # DA에서는 router에서 넘어오는 세션을 사용하면 됨
    session_id = random.randrange(1, 99999999)

    print "<Model Group List>"
    modelGroupList = resolver_stub.GetModelGroups(empty)
    for mg in modelGroupList.groups:
        split_mg_name = mg.name[:mg.name.rfind('_')]
        flag = mg.name[mg.name.rfind('_') + 1:]
        split_mg_name = split_mg_name + "(" + flag + ")"
        print "\n>>>>", str(split_mg_name)

    model_group_name = raw_input("\n[Select model group] ")

    MG = resolver_pb2.ModelGroup()
    MG.name = model_group_name
    MG.lang = lang
    MG.is_external = is_external
    MG.is_education = is_education

    '''
    Find: 대화할 sds 서버가 실행되어 있는지 확인하여 sds 서버의 주소 return
        실행되어 있지 않다면 새로 실행시킨다.
    '''
    server_status = resolver_stub.Find(MG)
    print 'find result'
    print server_status
    # Find 호출하여 받은 정보로 sds 서버 stub 생성
    sds_chan  = grpc.insecure_channel(server_status.server_address)
    print sds_chan
    sds_stub = sds_pb2_grpc.SpokenDialogServiceInternalStub(sds_chan)

    time.sleep(2)
    print "<Model List>"
    modelList = sds_stub.GetCurrentModels(empty)
    for ml in modelList.models:
        print str(ml)

    model_name = raw_input("[Select model] ")

    print "Session ID : " + str(session_id)
    print "Model : " + model_name

    # Create OpenParam & set session_key
    dp = sds_pb2.DialogueParam()
    dp.session_key = session_id
    dp.model = model_name
    dp.user_initiative = user_initiative

    print "<Dialog Param>"
    print dp

    '''
    Dialog Open
    Open: 세션을 생성한다.
    '''
    sds_session = sds_stub.Open(dp)
    print "open success"

    '''
    Dialog: 대화를 진행한다.
        사용자 발화를 분석하고, 시스템 응답 메세지를 준다.
    '''
    sdsUser = sds_pb2.SdsQuery()
    sdsUser.session_key = session_id
    sdsUser.model = model_name
    print("Hello!")

    user_utter = raw_input("[User]: ")
    while (user_utter != "exit"):

        sdsUser.utter = user_utter
        sdsResponse = sds_stub.Dialog(sdsUser)
        print "<DIALOG RESULT>"
        #print sdsResponse

        if (not sdsResponse.response):
            break

        print "[System] " + sdsResponse.response

        user_utter = raw_input("[User]  ")

    '''
    Close: 대화가 끝나면 호출하여 세션을 닫아준다.
    '''
    sds_stub.Close(dp)

    print("SDS is closed")

    '''
    Stop: sds 서버를 종료한다.
    '''
    #resolver_stub.Stop(MG)

    print("SDSD is now closed")


if __name__ == '__main__':
    serve()

