#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

# 기본 모듈
from concurrent import futures
import time
import argparse
import grpc
from google.protobuf import empty_pb2
import os

# 추가 모듈
import random
# import pymysql

# Path
exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

from maum.m2u.facade import userattr_pb2
from maum.m2u.da import provider_pb2
from maum.brain.sds import sds_pb2
from maum.brain.sds import resolver_pb2
from common.config import Config

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

class SampleDA(provider_pb2.DialogAgentProviderServicer):
    # STATE
    # state = provider_pb2.DIAG_STATE_IDLE
    init_param = provider_pb2.InitParameter()

    # PROVIDER
    provider = provider_pb2.DialogAgentProviderParam()
    provider.name = 'sample'
    provider.description = 'sample'
    provider.version = '0.1'
    provider.single_turn = False
    provider.agent_kind = provider_pb2.AGENT_SDS
    provider.require_user_privacy = True

    # SDS Stub
    sds_server_addr = ''
    sds_stub = None

    ####################################################################
    ### SDS 2.0
    ### lang: 한국어일 경우 0, 영어일 경우 1
    ### is_external: 외부 DB를 쓰는 경우 True, 내부 DB만 쓰는 경우 False
    ### model_list: 열고 싶은 모델들의 리스트
    #####################################################################
    lang = 0
    is_external = True
    model_list = ['AI_Agent', 'base']

    def __init__(self):
        self.state = provider_pb2.DIAG_STATE_IDLE

    #
    # INIT or TERM METHODS
    #
    def get_sds_server(self):
        sds_channel = grpc.insecure_channel('0.0.0.0:9860')
        resolver_stub = resolver_pb2.SdsServiceResolverStub(sds_channel)


        ### For SDS 2.0

        MG = resolver_pb2.ModelGroup()
        MG.name = 'sample' ## TODO: FIX ME!!
        MG.lang = self.lang
        MG.is_external = self.is_external

        resolver_stub.CreateModelGroup(MG)

        Model = resolver_pb2.Model()
        Model.lang = self.lang
        Model.is_external = self.is_external

        MP = resolver_pb2.ModelParam()
        MP.lang = self.lang
        MP.is_external = self.is_external
        MP.group_name = MG.name

        ML = resolver_pb2.ModelList()

        for mn in self.model_list:
            Model.name = mn
            MP.model_name = mn

            Model = ML.models.add()
            resolver_stub.LinkModel(MP)

        server_status = resolver_stub.Find(MG)
        print 'find result', server_status

        # Create SpokenDialogService Stub
        print 'find result loc: ', server_status.server_address

        self.sds_stub = sds_pb2.SpokenDialogServiceStub(
            grpc.insecure_channel(server_status.server_address))

        self.sds_server_addr = server_status.server_address
        print 'stub sds ', server_status.server_address

    def IsReady(self, empty, context):
        print 'IsReady', 'called'
        status = provider_pb2.DialogAgentStatus()
        status.state = self.state
        return status

    def Init(self, init_param, context):
        print 'Init', 'called'
        self.state = provider_pb2.DIAG_STATE_INITIALIZING
        # COPY ALL
        self.init_param.CopyFrom(init_param)
        # DIRECT METHOD
        self.sds_path = init_param.params['sds_path']
        print 'path'
        self.sds_model = init_param.params['sds_model']
        print 'model'

        # CONNECT
        self.get_sds_server()
        print 'sds called'
        self.state = provider_pb2.DIAG_STATE_RUNNING
        # returns provider
        result = provider_pb2.DialogAgentProviderParam()
        result.CopyFrom(self.provider)
        print 'result called'
        return result

    def Termiminate(self, empty, context):
        print 'Terminate', 'called'
        # DO NOTHING
        self.state = provider_pb2.DIAG_STATE_TERMINATED
        return empty_pb2.Empty()

    #
    # PROPERTY METHODS
    #
    def GetProviderParameter(self, empty, context):
        print 'GetProviderParameter', 'called'
        result = provider_pb2.DialogAgentProviderParam()
        result.CopyFrom(self.provider)
        return result

    def GetRuntimeParameters(self, empty, context):
        print 'GetRuntimeParameters', 'called'
        params = []
        result = provider_pb2.RuntimeParameterList()

        sds_path = provider_pb2.RuntimeParameter()
        sds_path.name = 'sds_path'
        sds_path.type = userattr_pb2.DATA_TYPE_STRING
        sds_path.desc = 'DM Path'
        sds_path.default_value = 'sample'
        sds_path.required = True
        params.append(sds_path)

        sds_model = provider_pb2.RuntimeParameter()
        sds_model.name = 'sds_model'
        sds_model.type = userattr_pb2.DATA_TYPE_STRING
        sds_model.desc = 'DM Model'
        sds_model.default_value = 'sample'
        sds_model.required = True
        params.append(sds_model)

        result.params.extend(params)
        return result

    #
    # DIALOG METHODS
    #
    def Talk(self, talk, context):
        self.get_sds_server()

        #######################################################
        ### SDS 2.0: model_list에 있는 모델을 1. 하나만 열거나,
        ### 2. 순차적으로 열거나,
        ### 3. 한번에 다 열수도 있다.
        ### 주어진 두 방법은 예시이며 모델을 열고 닫는 것은 충분히
        ### 조정할수 있다
        ########################################################

        empty = empty_pb2.Empty()
        cML = sds_stub.GetCurrentModels(empty)
        print 'current model list:'
        print cML

        aML = sds_stub.GetAvailableModels(empty)
        print 'available model list:'
        print aML
        session_id = talk.session_id

        ### 1. 하나만 열기

        dp = sds_pb2.DialogueParam()
        dp.model = self.model_list[0]
        dp.session_key = session_id
        dp.user_initiative = True ## user 가 먼저 발화하면 true, system발화가 먼저면 false

        OpenResult = sds_stub.Open(dp)
        print OpenResult

        sq = sds_pb2.SdsQuery()
        sq.model = dp.model
        sq.session_key = dp.session_key
        sq.apply_indri_score = 0
        sq.utter = talk.text

        intent = sds_stub.Understand(sq)
        print intent ## SdsAct가 Intent로 바뀜

        talk_res = provider_pb2.TalkResponse()

        entities = sds_pb2.Entities()
        entities.session_key = dp.session_key
        entities.model = dp.model

        sds_utter = sds_stub.GenerateEntities(entities)
        print sds_utter

        print sds_utter.response
        talk_res.text = sds_utter.response

        if sds_utter.status == 'end':
            sds_stub.Close(dp)

        return talk_res
        '''
        ### 2. 순차적으로 열기. 첫번째 도메인이 끝나면 다음 도메인 열기

        for i in range(0, len(self.model_list)):
            dp = sds_pb2.DialogueParam()
            dp.model = self.model_list[i]
            dp.session_key = session_id
            dp.user_initiative = True 

            OpenResult = sds_stub.Open(dp)
            print OpenResult

            sq = sds_pb2.SdsQuery()
            sq.model = dp.model
            sq.session_key = dp.session_key
            sq.apply_indri_score = 0
            sq.utter = talk.text

            intent = sds_stub.Understand(sq)
            print intent ## SdsAct가 Intent로 바뀜

            entities = sds_pb2.Entities()
            entities.session_key = dp.session_key
            entities.model = dp.model

            talk_res = provider_pb2.TalkResponse()
            sds_utter = sds_stub.GenerateEntities(entities)
            print sds_utter

            print sds_utter.response
            talk_res.text = sds_utter.response

            if sds_utter.staus == 'end':
                sds_stub.Close(dp)
                if i + 1 < [self.model_list]
                dp.model = self.model_list[i + 1]

            return talk_res
        '''

        '''
        ### 3. 한꺼번에 열기

        dp = sds_pb2.DialogueParam()
        dp.session_key = session_id
        dp.user_initiative = True 

    
        sq = sds_pb2.SdsQuery()
        sq.session_key = dp.session_key
        sq.apply_indri_score = 0
        sq.utter = talk.text

        entities = sds_pb2.Entities()
        entities.session_key = dp.session_key

        confidence = 0

        talk_res = provider_pb2.TalkResponse()

        for mn in self.model_list:
            dp.model = mn
            OpenResult = sds_stub.Open(dp)
            print OpenResult

            sq.model = mn
            intent = sds_stub.Understand(sq)
            
            print intent 

            entities.model = dp.model

            sds_utter = sds_stub.GenerateEntities(entities)
            print sds_utter.response
            
            if sds_utter.confidence > confidence:
                talk_res.text = sds_utter.response ## Confidence가 가장 높은 값을 response 로 지정


        print talk_res.text

        return talk_res
        '''

    def Close(self, req, context):
        print 'Closing for ', req.session_id, req.agent_key
        talk_stat = provider_pb2.TalkStat()
        talk_stat.session_key = req.session_id
        talk_stat.agent_key = req.agent_key

        ses = sds_pb2.DialogueParam()
        ses.session_key = req.session_id
        ### 1.
        ses.model = self.model_list[0]
        self.sds_stub.Close(ses)

        '''
        ### 2.
        ses.model = self.model_list[len(self.model_list) - 1]
        self.
        '''

        '''
        ### 3.
        for mn in self.model_list:
            ses.model = mn
            self.sds_stub.Close(ses)
        '''

        return talk_stat

def serve():
    parser = argparse.ArgumentParser(description='SampleDA')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        required=True,
                        type=int,
                        help='port to access server')
    args = parser.parse_args()
    conf = Config()
    data = [
        ('grpc.max_connection_idle_ms', int(conf.get("maum.sds.externaldb.timeout"))),
        ('grpc.max_connection_age_ms', int(conf.get("maum.sds.externaldb.timeout")))
    ]
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10), None, data, None)
    provider_pb2.add_DialogAgentProviderServicer_to_server(
        SampleDA(), server)

    listen = '[::]' + ':' + str(args.port)
    server.add_insecure_port(listen)

    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)

if __name__ == '__main__':
    serve()
