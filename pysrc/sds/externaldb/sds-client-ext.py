#! /usr/bin/env python
# -*- coding: utf-8 -*-
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
import os
import grpc
import argparse
import random

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

from google.protobuf import empty_pb2
from maum.brain.sds import sds_pb2
from maum.brain.sds import sds_pb2_grpc


#
# DIALOG METHODS
#
def serve():
    parser = argparse.ArgumentParser(description='dialExternalDB Test')
    parser.add_argument('-e', '--endpoint',
                        nargs='?',
                        dest='endpoint',
                        required=True,
                        type=str,
                        help='server to access SDS')
    args = parser.parse_args()

    sds_stub = sds_pb2_grpc.SpokenDialogServiceStub(
        grpc.insecure_channel(args.endpoint))
    empty = empty_pb2.Empty()

    print "<Model List>"
    modelList = sds_stub.GetCurrentModels(empty)
    for ml in modelList.models:
        print str(ml)

    model = raw_input("[Select model] ")

    session_id = random.randrange(1, 999999999)
    print "Session ID : " + str(session_id)
    print "Model : " + str(model)

    # Create OpenParam & set session_key
    dp = sds_pb2.DialogueParam()
    dp.session_key = session_id
    dp.model = model

    # Dialog Open
    sdsSession = sds_stub.Open(dp)

    sdsUser = sds_pb2.SdsQuery()
    sdsUser.session_key = session_id
    sdsUser.model = model

    user_utter = raw_input("[User]  ")
    while (user_utter != "exit"):

        sdsUser.utter = user_utter
        sa = sds_stub.Understand(sdsUser)

        entities = sds_pb2.Entities()
        entities.session_key = session_id
        entities.model = model
        sdsResponse = sds_stub.GenerateEntities(entities)

        if (not sdsResponse.response):
            print "not exist system response"
            break

        print "[System] " + sdsResponse.response

        user_utter = raw_input("[User]  ")

    sds_stub.Close(dp)

    print("SDS is closed")


if __name__ == '__main__':
    serve()
