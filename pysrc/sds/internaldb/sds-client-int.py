#! /usr/bin/env python
# -*- coding: utf-8 -*-
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
import os
import grpc
import argparse
import random

from concurrent import futures
import time

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)
print bin_path

from google.protobuf import empty_pb2
from maum.brain.sds import sds_pb2
from maum.brain.sds import sds_pb2_grpc


#
# DIALOG METHODS
#


def serve():
    parser = argparse.ArgumentParser(description='dialInternalDB Test')
    parser.add_argument('-e', '--endpoint',
                        nargs='?',
                        dest='endpoint',
                        required=True,
                        type=str,
                        help='server to access SDS')

    args = parser.parse_args()

    sds_stub = sds_pb2_grpc.SpokenDialogServiceInternalStub(
        grpc.insecure_channel(args.endpoint))
    empty = empty_pb2.Empty()

    print "<Model List>"
    modelList = sds_stub.GetCurrentModels(empty)
    for ml in modelList.models:
        print str(ml)

    model = raw_input("[Select model] ")

    session_id = random.randrange(1, 999999999)

    # Create DialogSessionKey & set session_key
    dp = sds_pb2.DialogueParam()
    dp.session_key = session_id
    dp.model = model
    dp.user_initiative = False

    # Dialog Open
    OpenResult = sds_stub.Open(dp)

    print(OpenResult.sds_response.response)

    sdsUser = sds_pb2.SdsQuery()
    sdsUser.session_key = session_id
    sdsUser.model = model
    user_utter = raw_input("[User]  ")
    sdsUser.utter = user_utter
    Dialog = sds_stub.Dialog(sdsUser)

    while (Dialog.finished != False):
        if (not Dialog.response):
            print "not exist system response"
            break

        print Dialog.response

        user_utter = raw_input("[User] ") + "\n"

        sdsUser.utter = user_utter

        Dialog = sds_stub.Dialog(sdsUser)

    dsk = sds_pb2.DialogueParam()
    dsk.session_key = session_id
    dsk.model = model

    sds_stub.Close(dsk)

    print("SDS is now closed")


if __name__ == '__main__':
    serve()
