## [2.0.2](https://github.com/mindslab-ai/brain-sds/compare/v2.0.1...v2.0.2) (2017-12-22)

### Bug Fixes

* **resolver**: 자식 프로세스 이름 변경된 반영 안되어 프로세스 시작 오류
* **sub-sds**: SDS 로그 파일 이름과 로그에 남는 프로세스 이름 변경, brain-sds로 변경

None

### Features

None

### Enhancements

* **config**: supervisor에서 사용하는 이름 brain-sds로 변경
None

### Breaking Changes

None


## [2.0.1](https://github.com/mindslab-ai/brain-sds/compare/v2.0.0...v2.0.1) (2017-12-08)

### Bug Fixes

None

### Features

* **proto**: 네임스페이스 변경, minds/sds -> `maum.brain.sds`로 재정리
* **resolver**: `-v` 옵션을 통해서 버전 정보를 출력

### Enhancements

None

### Breaking Changes

* 네임스페이스가 재정리되어서 기존 버전과의 호환되지 않으므로 주의 필요

# [2.0.0](https://github.com/mindslab-ai/brain-sds/compare/507899c...v2.0.0) (2017-10-31)

<!---
 MAJOR.MINOR 패치는 # 즉, H1으로 표시
 MAJOR>MINOR.PATCH는 ## 즉, H2로 표시한다.
--->

### Bug Fixes

* **sub_sdsd**: darun 중 fillslots 에서 child가 죽는 이슈 [#31](https://github.com/mindslab-ai/brain-sds/issues/31)
* **sub_sdsd**: 한국어 sds일때 encoding 문제 [#28](https://github.com/mindslab-ai/brain-sds/issues/28)
* **resolver**: (common) 지속적으로 sds child가 생성되는 문제 [#23](https://github.com/mindslab-ai/brain-sds/issues/23)
* **sub_sdsd**: (common) internal impl Dialog() 오류, log부분 수정 [#21](https://github.com/mindslab-ai/brain-sds/issues/21)

### Features

* **proto**: protoBuf
* **config**: 중복 사용되는 학습 이미지의 경우, Symbolic Link 지원. 학습이미지 배포 디렉토리 구조 체계화.
* **proto**: 2.0 버전에서 용어 변경. sds.proto와 resolver.proto로 분리
* **resolver**: 대화 model 관리기능(model group 관리 등) 추가.
* **sub_sdsd**: External, Internal 구분 및 한글과 영어 구분에 따른 4가지 type의 binary로 구현.
           하나의 sds process가 model group에 속한 여러 model들 서비스 가능.
* **pysrc**: DA Test를 위한 Python 소스

### Enhancements

* **resolver**: 학습이미지 trained 구조 변경에 따른 새 함수 테스트 [#43](https://github.com/mindslab-ai/brain-sds/issues/43)
* **proto**: 용어 재정의 및 trained 구조 변경 [#39](https://github.com/mindslab-ai/brain-sds/issues/39)
* **proto**: impl 개선 [#11](https://github.com/mindslab-ai/brain-sds/issues/11)
* **build.sh**: minds-sds에 systemd.system 추가 / build.sh 수정 [#10](https://github.com/mindslab-ai/brain-sds/issues/10)
* **resolver**: resolver에 domain 관리기능 추가 [#9](https://github.com/mindslab-ai/brain-sds/issues/9)
* **config**: docker volume도 resource로 별도 관리 [#7](https://github.com/mindslab-ai/brain-sds/issues/7)
* **proto**: (common) session key 변경 [#5](https://github.com/mindslab-ai/brain-sds/issues/5)

### Breaking Changes

None
