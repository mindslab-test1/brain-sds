#ifndef _SDS_ENCODING_H
#define _SDS_ENCODING_H

#include <libmaum/common/encoding.h>

inline std::string ToUtf8(const std::string &src) {
#if defined(LANG_KOR)
  return Cp949ToUtf8(src);
#else
  return src;
#endif
}

inline std::string ToCp949(const std::string &src) {
#if defined(LANG_KOR)
  return Utf8ToCp949(src);
#else
  return src;
#endif
}

#endif //_SDS_ENCODING_H
