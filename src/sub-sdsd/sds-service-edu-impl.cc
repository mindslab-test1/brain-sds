#include <fstream>
#include <vector>
#include <iostream>
#include <grpc++/grpc++.h>
#include <libmaum/common/config.h>
#include <libmaum/log/call-log.h>
#include <libmaum/rpc/result-status.h>
#include <dirent.h>

#include "sds-encoding.h"
#include "sds-service-edu-impl.h"

using namespace google::protobuf;
using std::string;
using grpc::ServerBuilder;
using grpc::Server;
using grpc::Service;
using google::protobuf::Map;
using std::unique_ptr;
using libmaum::log::CallLogInvoker;
using libmaum::rpc::CommonResultStatus;

/**
 * 새로운 child process의 메인 함수
 *
 * Dialog Service 시스템을 초기화하고 정상적으로 동작하도록 한다.
 *
 * @param name 도메인 이름
 * @param path DB 경로
 * @param endpoint 응답 엔드포인트
 * @param port 서버 포트
 */

// error_index: 00000 ~ 09999
#define ERROR_INDEX_MAJOR 30000

extern int ApplyIndriScore;
void RunSdsChild(const string &group,
                 const string &path,
                 const string &endpoint) {

  libmaum::Config &c = libmaum::Config::Instance();
  auto logger = c.GetLogger();
  long grpc_timeout = c.GetDefaultAsInt("maum.sds.timeout", "0");

  SdsServiceImpl service(path, endpoint);

  ServerBuilder builder;
  if (grpc_timeout > 0) {
    builder.AddChannelArgument(GRPC_ARG_MAX_CONNECTION_IDLE_MS, grpc_timeout);
  }
  builder.AddListeningPort(endpoint, grpc::InsecureServerCredentials());

  builder.RegisterService(&service);

  unique_ptr<Server> server(builder.BuildAndStart());
  if (server) {
    std::thread init([&]() {
      logger->info("SDS Server initializing");
      service.Initialize();
      logger->info("SDS Server [{}] listening on {}", group, endpoint);
      logger->flush();
    });
    init.detach();
    server->Wait();
  }
}

/**
 * 생성자
 *
 * 대화 시스템을 초기화한다.
 *
 * @param name 도메인 이름
 * @param path DB 경로
 * @param endpoint 응답 엔드포인트
 */
SdsServiceImpl::SdsServiceImpl(const string &path, const string &endpoint)
    : path_(path), endpoint_(endpoint) {
}

void SdsServiceImpl::Initialize() {
  state_ = maum::brain::sds::ServerState::SERVER_STATE_INITIALIZING;

  auto logger = LOGGER();
  auto &c = libmaum::Config::Instance();

  const string &path = path_;
  logger->debug("path_: {}", path);

  string resource;
  string flag = path.substr(path.length() - 3);
  lang_ = "e";

  string apply_indri_score = c.Get("brain.sds.apply.indri.score");
  expire_interval_ = c.GetDefaultAsInt("maum.sds.session.monitor.expiration",
                                       "1800");

  if (strcmp(apply_indri_score.c_str(), "true") == 0) {
    ApplyIndriScore = 1;
  }

  logger->debug("apply_indri_score option is {}", apply_indri_score);

  resource = c.Get("resources.edu.dir");
  logger->debug("resource is located at {}.", resource);

  // Creating nlp_dict symbolic link
  string target_nlp = path + "/nlp_dict";
  string resource_nlp = resource + "/nlp_dict";

  if (access(target_nlp.c_str(), R_OK | W_OK | F_OK) != 0) {
    logger->warn("cannot access nlp_dict. resource is being linked");
    int sym_link_nlp = symlink(resource_nlp.c_str(), target_nlp.c_str());

    if (sym_link_nlp < 0) {
      logger->warn("nlp_dict: cannot symlink {} from: {} to: {}",
                   errno, resource_nlp, target_nlp);
    } else {
      logger->debug("chatbot_dict: symlink is successfully created");
    }
  }

  // Creating chatbot_dict symbolic link
  string target_cb = path + "/chatbot_dict";
  string resource_cb = resource + "/chatbot_dict";

  if (access(target_cb.c_str(), R_OK | W_OK | F_OK) != 0) {
    logger->warn("cannot access chatbot_dict. resource is being linked");
    int sym_link_cb = symlink(resource_cb.c_str(), target_cb.c_str());
    if (sym_link_cb < 0) {
      logger->warn("chatbot_dict: cannot symlink {} from: {} to: {}",
                   errno, resource_cb, target_cb);
    } else {
      logger->debug("chatbot_dict: symlink is successfully created");
    }
  }

#if 0
  if (CheckUpdate()) {
  }
#endif

  string target_d = path + "/dialog_domain";
  string list = target_d + "/domain_list.txt";

  DIR *dir = opendir(target_d.c_str());

  if (dir == nullptr) {
    logger->warn("dialog_domain: cannot open directory {}", target_d);
  } else {
    if (access(list.c_str(), F_OK) == 0) {
      logger->debug("Found existing file: {}. Updating...", list);
      int result = unlink(list.c_str());
      if (result < 0) {
        logger->warn("Could not unlink {}.", list);
      } else {
        logger->debug("Successfully unlinked {}", list);
      }
    }
    std::ofstream outfile(list.c_str());
    struct dirent *ent = nullptr;

    while ((ent = readdir(dir))) {
      if (strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0
          && strcmp(ent->d_name, "domain_list.txt") != 0) {
        outfile << ent->d_name << std::endl;
      }
    }
    closedir(dir);
    outfile.close();
  }

  if (!open_DialogSystem_and_Domain_KB(path)) {
    logger->warn("failed initialize server from open KB [path: {}]", path);
    logger->warn("if path is correct then can exist wrong model");
    logger->flush();
  } else {
    logger->debug("[path: {}] open success", path);
    initialized_ = true;
  }
  state_ = maum::brain::sds::ServerState::SERVER_STATE_RUNNING;

  std::thread([=]() {
    while (true) {
      CleanDisabledSession();
      sleep(30);
    }
  }).detach();
}

void SdsServiceImpl::CleanDisabledSession() {
  string::size_type m_pos;
  ServerContext context;
  google::protobuf::Empty response;

  std::lock_guard<std::recursive_mutex> guard(session_lock_);

  vector<string> keys(session_times_.size());
  transform(session_times_.begin(), session_times_.end(), keys.begin(),
            [](std::pair<string, time_t> pair) { return pair.first; });

  for (auto &key : keys) {
    int64_t diff_time = time(nullptr) - session_times_[key];

    if (diff_time > expire_interval_) {
      DialogueParam dialogue_param;
      m_pos = key.find(":");
      string model = key.substr(0, m_pos);
      int64_t session_key = std::stol(
          key.substr(m_pos + 1, key.length() - m_pos - 1).c_str());
      LOGGER_debug("delete model name: [{}], session_key: [{}]",
                   model,
                   session_key);

      dialogue_param.set_model(model);
      dialogue_param.set_session_key(session_key);

      Close(&context, &dialogue_param, &response);
    }
  }
}

/**
 * 소멸자
 *
 * 대화 시스템을 종료한다.
 */
SdsServiceImpl::~SdsServiceImpl() {
  if (initialized_)
    close_DialogSystem_and_Domain_KB(); //fixed!!
  LOGGER_warn("stopping path_ {}", path_);
}

bool SdsServiceImpl::CheckUpdate() {
  bool updated;
  std::vector<std::string> directory, txt_list;

  string path = path_;
  path += "/dialog_domain";
  DIR *dir;
  struct dirent *ent = nullptr;

  dir = opendir(path.c_str());
  while ((ent = readdir(dir))) {
    if (strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0
        && strcmp(ent->d_name, "domain_list.txt") != 0) {
      string model_name = ent->d_name;
      directory.push_back(model_name);
    }
  }
  closedir(dir);

  sort(directory.begin(), directory.end());

  path += "/domain_list.txt";
  string line;
  std::ifstream openFile(path.c_str());
  while (getline(openFile, line)) {
    txt_list.push_back(line);
  }
  openFile.close();

  sort(txt_list.begin(), txt_list.end());

  if (directory.size() == txt_list.size()) {
    updated = !equal(directory.begin(), directory.end(), txt_list.begin());
    LOGGER_debug("updated? {}", updated);
  } else {
    updated = true;
  }
  return updated;
}

void SdsServiceImpl::SetTlsId(const int64_t session_id,
                              const ServerContext *context) {
  ResetTlsValues();

  if (session_id) {
    SetTlsSessionId(session_id);
  }

  const auto &client_map = context->client_metadata();
  auto item_s = client_map.find("x-operation-sync-id");
  if (item_s != client_map.end()) {
    string xid = string(item_s->second.begin(), item_s->second.end());
    SetTlsOpSyncId(xid);
    LOGGER_info("operation from {}", context->peer());
  } else {
    LOGGER_warn("x-operation-sync-id is not found");
  }
}

/**
 * 대화 세션을 생성한다.
 *
 * @param context 서버 컨텍스트
 * @param request 빈 메시지
 * @param response 새로운 새션 정보
 * @return 호출 성공여부
 * 현재 유저 발화가 먼저인 경우는 없다.
 */
Status SdsServiceImpl::Open(ServerContext *context,
                            const DialogueParam *request,
                            OpenResult *response) {

  Status status(Status::OK);
  CallLogInvoker log(__func__, context, request, response, &status);

  if (!request->session_key()) {
    LOGGER_warn("Not exist session_key. Please set session_key");
    status = ResultGrpcStatus(ExCode::INVALID_ARGUMENT,
                              ERROR_INDEX(2001),
                              "Not exist session_key. Please set session_key");
    return status;
  }

  SetTlsId(request->session_key(), context);
  LOGGER_trace("Open request: [{}]", request->Utf8DebugString());

  // 새로운 DialogSession 생성
  auto logger = LOGGER();
  int64_t session_key = request->session_key();
  const string &model = request->model();
  string key = model + ":" + std::to_string(session_key);
  string system_dialog;
  string mission_id = "-1";
  int type = 0;

  std::lock_guard<std::recursive_mutex> guard(session_lock_);

  auto dialog = find(key);
  if (dialog == nullptr) {
    auto dialog = new DialogSession;
    logger_debug("create dialog session: {} for {}", (void *) dialog, key);
    int rc;

    logger_debug("open dialog session model:{} mission_id:{}, type:{}",
                 model, mission_id, type);
    rc = dialog->open(model, mission_id, type);

    if (!rc) {
      logger_error("Cannot open dialog session {}",
                   dialog->get_error_message());
      logger->flush();
      delete dialog;
      response->mutable_sds_stat()->set_success(false);
      response->mutable_sds_stat()->set_detail_message(
          "Cannot open new dialog session");
      status = ResultGrpcStatus(ExCode::RUNTIME_ERROR,
                                ERROR_INDEX(2002),
                                "Cannot open dialog session %s",
                                dialog->get_error_message().c_str());
      return status;
    }

    // 생성된 DialogSession 을 session_id와 매핑하여 등록
    insert(key, dialog);
    logger_debug("session size = {}", sessions_.size());
  } else {
    logger_debug("use prev session {} for {}", (void *) dialog, key);
  }

  response->mutable_sds_stat()->set_success(true);
  response->mutable_sds_stat()->set_detail_message(
      "Success open new dialog session");
  response->mutable_sds_stat()->set_system_dialog(system_dialog);

  logger_trace("Open response: [{}]", response->Utf8DebugString());

  return status;
}

/**
 * 대화 세션을 닫는다.
 *
 * @param context 서버 컨텍스트
 * @param request 세션 정보
 * @param response 빈 메시지
 * @return 호출 성공 여부
 */
Status SdsServiceImpl::Close(ServerContext *context,
                             const DialogueParam *request,
                             ::google::protobuf::Empty *empty) {

  Status status(Status::OK);
  CallLogInvoker log(__func__, context, request, empty, &status);

  if (!request->session_key()) {
    LOGGER_warn("Not exist session_key. Please set session_key");
    status = ResultGrpcStatus(ExCode::INVALID_ARGUMENT,
                              ERROR_INDEX(4001),
                              "Not exist session_key. Please set session_key");
    return status;
  }

  SetTlsId(request->session_key(), context);
  LOGGER_trace("Close request: [{}]", request->Utf8DebugString());

  int64_t session_key = request->session_key();
  const string &model = request->model();
  string key = model + ":" + std::to_string(session_key);

  std::lock_guard<std::recursive_mutex> guard(session_lock_);

  auto dialog = remove(key);
  if (dialog) {
    auto logger = LOGGER();
    int close_result = dialog->close();
    logger_debug("close result: {}", close_result);
    if (!close_result) {
      logger_error("Cannot close dialog session {}",
                   dialog->get_error_message());
      logger->flush();
    }
    logger_debug("session size = {}", sessions_.size());
    logger_debug("session deleted {} for {}", (void *) dialog, key);
    delete dialog;
  } else {
    LOGGER_debug("session not found {}, {}", model, session_key);
  }

  return status;
}

/**
 * 서버가 살아있는지 점검한다.
 *
 * @param context 서버 컨텍스트
 * @param request 서버 접속 기본 정보
 * @param response 서버 접속 엔드포인트
 * @return 호출 성공 여부
 */
Status SdsServiceImpl::Ping(ServerContext *context,
                            const ModelGroup *request,
                            ServerStatus *response) {

  Status status(Status::OK);
  CallLogInvoker log(__func__, context, request, response, &status);
  SetTlsId(atoi(""), context);
  LOGGER_trace("Ping Request: [{}]", request->Utf8DebugString());

  auto found = path_.find_last_of('/');
  string dir_path = path_.substr(found + 1);
  string group_name = request->name();
  group_name += "_edu";

  if (group_name == dir_path) {
    response->set_group_name(request->name());
    response->set_server_address(endpoint_);
    response->set_lang(request->lang());
    response->set_is_external(false);
    response->set_is_education(true);
    if (CheckUpdate()) {
      response->set_state(maum::brain::sds::SERVER_STATE_UPDATING);
    } else {
      response->set_state(state_);
    }
    response->set_invoked_by("pong");
  } else {
    LOGGER_debug("{} invalid path {} : but this {}",
                 getpid(),
                 request->name(),
                 dir_path);
    response->set_state(maum::brain::sds::SERVER_STATE_NONE);
    status = ResultGrpcStatus(ExCode::INVALID_ARGUMENT,
                              ERROR_INDEX(1001),
                              "invalid path about %s",
                              request->name().c_str());
    return status;
  }
  LOGGER_trace("Ping response: [{}]", response->Utf8DebugString());
  return status;
}

/**
 * SDS에서 출력된 SLU set에서 형태소 태깅 결과를 파싱한다.
 *
 * @param slu SDS에서 출력된 SLU Set
 * return :
 *   success  <MA> “표층형/대표형/품사/의미코드(NE정보)” </MA>
 *     ex) <MA> 삼성폰/삼성폰/NN.용언불가능/통신기기 최저가/최저가/NN.용언불가능/가치 </MA>
 *   fail  empty string
 **/
string SdsServiceImpl::GetPosTag(const string &slu) {
  auto logger = LOGGER();
  string::size_type s_pos, e_pos;
  string pos_tagging;

  s_pos = slu.find("<MA>") + 5;
  e_pos = slu.find("</MA>");
  if (s_pos != string::npos && e_pos != string::npos) {
    pos_tagging = slu.substr(s_pos, e_pos - s_pos);
  } else {
    logger_debug("Get Pos Tagging info failed.");
  }

  return pos_tagging;
}

/**
 * SDS에서 출력된 SLU set에서 Best SLU를 파싱한다.
 *
 * @param slu SDS에서 출력된 SLU Set
 * return :
 *   success  <SLU1> Best SLU </SLU1>
 *     ex) <SLU1> <Weather.date=내일> 날씨 어때	 #request(Weather.Info, Weather.date="내일") (0.844) </SLU1>
 *   fail  empty string
 **/
string SdsServiceImpl::GetBestSLU(const string &slu) {
  auto logger = LOGGER();
  string::size_type s_pos, e_pos;
  string best_slu;

  s_pos = slu.find("<SLU1>") + 7;
  e_pos = slu.find("</SLU1>");
  if (s_pos != string::npos && e_pos != string::npos) {
    best_slu = slu.substr(s_pos, e_pos - s_pos);
  } else {
    logger_debug("Get Best Slu fail, check DM.");
  }

  return best_slu;
}

/**
 * 대화 의도를 parsing 한다.
 *
 * @param slu GetBestSLU함수를 통해 출력된 Best SLU
 * return:
 *   success  intention string
 *     ex) request, unknown, inform 등
 *   fail  empty string
 **/
string SdsServiceImpl::GetDialogIntent(const string &slu) {
  auto logger = LOGGER();
  string::size_type s_pos, e_pos;
  s_pos = slu.find('#');
  e_pos = slu.find('(');
  if (e_pos != string::npos) {
    logger_debug("Get DI success");
    return slu.substr(s_pos + 1, e_pos - s_pos - 1);
  } else {
    LOGGER_debug("Get DI fail, input slu : {}", slu);
  }
  return "";
}

/**
 * internalDB
 */

Status SdsServiceImpl::Dialog(ServerContext *context,
                              const SdsQuery *request,
                              SdsResponse *response) {

  Status status(Status::OK);
  CallLogInvoker log(__func__, context, request, response, &status);
  auto logger = LOGGER();

  if (!request->session_key()) {
    logger_warn("Not exist session_key. Please set session_key");
    status = ResultGrpcStatus(ExCode::INVALID_ARGUMENT,
                              ERROR_INDEX(3001),
                              "Not exist session_key. Please set session_key");
    return status;
  }

  SetTlsId(request->session_key(), context);
  logger_trace("Dialog request: [{}]", request->Utf8DebugString());

  int64_t session_key = request->session_key();
  string model = request->model();
  string key = model + ":" + std::to_string(session_key);

  std::lock_guard<std::recursive_mutex> guard(session_lock_);

  auto dialog = find(key);
  if (dialog == nullptr) {
    logger_warn("invalid session key {}", key);
    status = ResultGrpcStatus(ExCode::INVALID_ARGUMENT,
                              ERROR_INDEX(3002),
                              "invalid session key");
    return status;
  }

  // 사용자 발화가 150자가 넘을 경우 예외 처리
  if (request->utter().length() > 1000) {
    LOGGER_warn("utter lengthis too long({})", request->utter().length());
    status = ResultGrpcStatus(ExCode::INVALID_ARGUMENT,
                              ERROR_INDEX(3004),
                              "user utter is too long");
    return status;
  }

  if (!dialog->dialog(ToCp949(request->utter()))) {
    logger_error("Dialog SLU Error: {}",
                 ToUtf8(dialog->get_error_message()));
    response->set_error_message(dialog->get_error_message());
    response->set_success(false);
    status = ResultGrpcStatus(ExCode::RUNTIME_ERROR,
                              ERROR_INDEX(3003),
                              "Dialog SLU Error: %s",
                              ToUtf8(dialog->get_error_message()).c_str());
    return status;
  }

  string sluSet = ToUtf8(dialog->m_user_dialog_act);
  logger_debug("[Original SLU Set] {}", sluSet);

  string posTagging = GetPosTag(sluSet);
  if (!posTagging.empty()) {
    logger_debug("[SLU POS Tagging] {}", posTagging);
    response->set_slu_pos_tagging(posTagging);
  }

  string bestSlu = GetBestSLU(sluSet);
  string di;
  if (!bestSlu.empty()) {
    logger_debug("[Best SLU] {}", bestSlu);
    response->set_origin_best_slu(bestSlu);

    di = GetDialogIntent(bestSlu);
    logger_debug("[Get DialogIntent] {}", di);
    response->set_intent(di);
  } else {
    logger_trace("sds internal status about bestSLU empty");
  }
  logger_flush();

  // `end`, `no end`가 오기 때문에 굳이 이를 UTF-8로 변경할 필요는 없어보입니다.
  response->set_finished(dialog->get_status() == "end");
  response->set_response(ToUtf8(dialog->get_system_response()));
  response->set_feedback_xml(dialog->get_turn_feedback_with_xml());
  response->set_success(true);

//  logger_trace("Dialog response: [{}]", response->Utf8DebugString());

  return status;
}

Status SdsServiceImpl::GetFeatures(ServerContext *context,
                                   const google::protobuf::Empty *empty,
                                   SdsFeatures *response) {

  Status status(Status::OK);
  CallLogInvoker log(__func__, context, empty, response, &status);

  response->add_features(FeatureList::SDS_INTERNAL);

  if (ApplyIndriScore == 1) {
    response->add_features(FeatureList::SDS_CONFIDENCE_INDRI_SCORE);
  } else {
    response->add_features(FeatureList::SDS_CONFIDENCE_SVM_SCORE);
  }
#if 0
  // TODO
  if (UserInitiative) {
    response->add_features(FeatureList::SDS_USER_INITIATIVE);
  } else {
    response->add_features(FeatureList::SDS_SYSTEM_INITIATIVE);
  }
#endif
  return status;
}

Status SdsServiceImpl::GetCurrentModels(ServerContext *context,
                                        const google::protobuf::Empty *empty,
                                        ModelList *response) {

  Status status(Status::OK);
  CallLogInvoker log(__func__, context, empty, response, &status);
  SetTlsId(atoi(""), context);
  auto logger = LOGGER();

  string path = path_ + "/dialog_domain/domain_list.txt";
  std::ifstream openFile(path.c_str());

  if (openFile.is_open()) {
    string line;
    while (getline(openFile, line)) {
      Model *model = response->add_models();
      maum::common::LangCode langCode;
      maum::common::LangCode_Parse("eng", &langCode);
      model->set_name(line);
      model->set_lang(langCode);
      model->set_is_external(false);
      model->set_is_education(true);
    }
    openFile.close();
  } else {
    logger_warn("cannot open domain_list.txt file");
    status = ResultGrpcStatus(ExCode::RUNTIME_ERROR,
                              ERROR_INDEX(8001),
                              "cannot open domain_list.txt file");
    return status;
  }

  LOGGER_trace("GetCurrentModels response: [{}]", response->Utf8DebugString());
  return status;
}

Status SdsServiceImpl::GetAvailableModels(ServerContext *context,
                                          const google::protobuf::Empty *empty,
                                          ModelList *response) {

  Status status(Status::OK);
  CallLogInvoker log(__func__, context, empty, response, &status);
  SetTlsId(atoi(""), context);
  auto &c = libmaum::Config::Instance();
  auto logger = LOGGER();

  string path = c.Get("models.dir");
  path += "/edu";

  logger_debug("GetAvailableModel path {}", path);
  DIR *dir = opendir(path.c_str());
  if (dir == nullptr) {
    logger_error("cannot open directory {}.", path);
    status = ResultGrpcStatus(ExCode::RUNTIME_ERROR,
                              ERROR_INDEX(8001),
                              "cannot open directory %s.",
                              path.c_str());
    return status;
  } else {
    struct dirent *ent = nullptr;
    while ((ent = readdir(dir))) {
      if (strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0) {
        char *model_name = ent->d_name;
        Model *model = response->add_models();
        maum::common::LangCode langCode;
        maum::common::LangCode_Parse("eng", &langCode);
        model->set_name(model_name);
        model->set_lang(langCode);
        model->set_is_external(false);
        model->set_is_education(true);
      }
    }
    closedir(dir);
  }

  logger_trace("GetAvailableModels response: [{}]",
               response->Utf8DebugString());
  return status;
}
