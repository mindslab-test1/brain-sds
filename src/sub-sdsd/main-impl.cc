#include "sds-service-int-impl.h"
// RunSdsChild 를 위한 함수, ext-impl.h에도 동일하게 적용되어 있으므로
// TODO, 옵티마이징이 필요한 상황입니다.

#include <libmaum/common/config.h>
#include <libmaum/common/util.h>
#include <libmaum/log/call-log.h>
#include <libmaum/rpc/result-status.h>

#include <sys/prctl.h>
#include <getopt.h>
#include <csignal>
#include <libgen.h>
#include <syslog.h>
#include <gitversion/version.h>
#include <dlfcn.h>

using grpc::Server;
using grpc::ServerBuilder;
using libmaum::rpc::CommonResultStatus;

using namespace std;
using std::shared_ptr;

volatile std::sig_atomic_t g_signal_status;

void HandleSignal(int signal) {
  // CALL DESTRUCTOR
  LOGGER()->info("handling {}", signal);
  g_signal_status = signal;
  exit(0);
}

void Usage(const char *prname) {
  cout
      << prname << '\n'
      << "[-d path] "
      #if defined(LANG_KOR)
      << "[-c encoding] "
      #endif
      << "[-e endpoint] [-p port] [-v] [-h]" << '\n'
      << "path : full-path, dm_base_path + \"/\" + query->path" << '\n'
      #if defined(LANG_KOR)
      << "encoding: [utf-8|cp949] *'utf-8' is default." << '\n'
      #endif
      << "endpoint : export_ip + \":\" + port" << '\n'
      << "interface port : port"
      << endl;

  exit(1);
}

extern void ChildHandler(int sig);

void LoadPlugins() {
  auto &c = libmaum::Config::Instance();
  auto plugin_count = c.GetAsInt("brain.sds.plugin.count");
  for (auto i = 0; i < plugin_count; i++) {
    std::string so_name = c.Get("brain.sds.plugin." + to_string(i + 1) + ".so");
    std::string
        entry = c.Get("brain.sds.plugin." + to_string(i + 1) + ".entrypoint");

    if (so_name.empty() || entry.empty()) {
      LOGGER()->warn("invalid so {} or entry {} ", so_name, entry);
      continue;
    }

    void *ptr = dlopen(so_name.c_str(), RTLD_NOW);
    if (ptr == nullptr) {
      LOGGER()->warn("cannot load shared object {}", so_name);
      continue;
    }
    void (*sym)();
    sym = reinterpret_cast<void (*)()>(dlsym(ptr, entry.c_str()));
    if (sym == nullptr) {
      LOGGER()->warn("cannot load sym {}", entry);
    }
    LOGGER()->info("calling {} {}", so_name, entry);
    sym();
  }
}

int main(int argc, char *argv[]) {

  auto &c = libmaum::Config::Init(argc, argv, "brain-sds.conf");

  CommonResultStatus::SetModuleAndProcess(maum::rpc::Module::BRAIN_SDS,
                                          maum::rpc::ProcessOfBrainSds::BRAIN_SDS_MAIN);

  std::signal(SIGINT, HandleSignal);
  //std::signal(SIGSEGV, HandleSignal);
  //std::signal(SIGBUS, HandleSignal);
  std::signal(SIGTERM, HandleSignal);

  const char *path = nullptr;
  const char *endpoint = nullptr;
  const char *port = nullptr;
#if defined(LANG_KOR)
  const char *enc = nullptr;
#endif
  bool do_exit = false;

  while (true) {
    int optindex = 0;
    static option sds_fork_option[] = {
        {"dir", required_argument, nullptr, 'd'},
#if defined(LANG_KOR)
        {"enc", optional_argument, nullptr, 'c'},
#endif
        {"endpoint", required_argument, nullptr, 'e'},
        {"port", required_argument, nullptr, 'p'},
        {"version", no_argument, nullptr, 'v'},
        {"help", no_argument, nullptr, 'h'}
    };
    int ch = getopt_long(argc, argv, "d:e:p:vh", sds_fork_option, &optindex);

    if (ch == -1)
      break;

    switch (ch) {
      default: {
        Usage(argv[0]);
        break;
      }
      case 'd': {
        path = optarg;
        break;
      }
#if defined(LANG_KOR)
      case 'c': {
        enc = optarg;
        break;
      }
#endif
      case 'e': {
        endpoint = optarg;
        break;
      }
      case 'p': {
        port = optarg;
        break;
      }
      case 'v': {
        printf("%s version %s\n", basename(argv[0]), version::VERSION_STRING);
        do_exit = true;
        break;
      }
      case 'h': {
        Usage(argv[0]);
        do_exit = true;
        break;
      }
    }
  }
  if (do_exit)
    exit(EXIT_SUCCESS);

  string group_name = path;
  string group_root = c.Get("groups.dir");
  if (group_name.find(group_root) != string::npos) {
    group_name = group_name.substr((group_root.length() + 1));
  }

  string process_name("brain-sds-" + group_name);

  c.SetProgramName(process_name);
  c.SetLoggerName(process_name);

  libmaum::log::CallLogger::GetInstance();

  LoadPlugins();

  auto logger = LOGGER();
  logger->debug("Start SDS Child Server");
  logger->debug("process_name : {}", process_name);
  int s = ::prctl(PR_SET_NAME, process_name.c_str(), NULL, NULL, NULL);
  snprintf(argv[0], strlen(argv[0]), "%s-%s",
           process_name.c_str(), port);
  logger->debug("[prctl status] {}", s);
  EnableCoreDump();

#if defined(LANG_KOR)
  {
    if (enc == nullptr) {
      enc = c.GetDefault("brain.sds.kor.knowledge.encoding", "utf-8").c_str();
    }
    DialogKnowledgeEncoding encoding = strcasecmp(enc, "cp949") == 0 ?
                                       CP949 :
                                       UTF8;
    SetKnowledgeEncoding(encoding);
    logger->info("Korean Knowledge Encoding: [{}].", encoding == UTF8 ?
                                                     "UTF-8" :
                                                     "CP949");
  }
#endif
  // run grpc server
  RunSdsChild(group_name, path, endpoint);

  return 0;
}

