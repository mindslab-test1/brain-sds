#ifndef _SDS_SERVICE_IMPL_H
#define _SDS_SERVICE_IMPL_H

#include <unordered_map>
#include <string>
using std::string;

#include "sdsapi.h"
#include "maum/brain/sds/sds.grpc.pb.h"
#include "../tls-value.h"

using grpc::ServerContext;
using grpc::Status;
using maum::brain::sds::SpokenDialogService;
using maum::brain::sds::Intent;
using maum::brain::sds::Entities;
using maum::brain::sds::SdsUtter;
using maum::brain::sds::SdsQuery;
using maum::brain::sds::DialogueParam;
using maum::brain::sds::OpenResult;
using maum::brain::sds::ServerStatus;
using maum::brain::sds::SdsFeatures;
using maum::brain::sds::Model;
using maum::brain::sds::ModelList;
using maum::brain::sds::ModelGroup;
using maum::brain::sds::FeatureList;
using maum::brain::sds::SdsStat;
using maum::brain::sds::Intent_EntityValue;
using maum::brain::sds::ServerState;
using namespace std;

class SdsServiceImpl : public SpokenDialogService::Service {
 public:
  explicit SdsServiceImpl(const string &path,
                          const string &endpoint);
  ~SdsServiceImpl() override;

  void Initialize();

  // check if server is alive and switch over if true.
  Status Ping(ServerContext *context,
              const ModelGroup *request,
              ServerStatus *response) override;
  // 새로운 세션을 생성한다.
  Status Open(ServerContext *context,
              const DialogueParam *request,
              OpenResult *response) override;
  // 세션을 종료한다.
  Status Close(ServerContext *context,
               const DialogueParam *request,
               google::protobuf::Empty *response) override;
  //return features
  Status GetFeatures(ServerContext *context,
                     const google::protobuf::Empty *empty,
                     SdsFeatures *features) override;
  //이용가능한 도메인 return
  Status GetCurrentModels(ServerContext *context,
                          const google::protobuf::Empty *empty,
                          ModelList *response) override;
  Status GetAvailableModels(ServerContext *context,
                            const google::protobuf::Empty *empty,
                            ModelList *response) override;

  // understand the context of user utterance.
  Status Understand(ServerContext *context,
                    const SdsQuery *request,
                    Intent *response) override;
  // contextualize and fill slots of user utterance.
  Status GenerateEntities(ServerContext *context,
                          const Entities *request,
                          SdsUtter *response) override;

  // contextualize and fill slots of user utterance.
  Status Generate(ServerContext *context,
                  const Entities *request,
                  SdsUtter *response) override;

  Status LoadModel(ServerContext *context,
                   const Model *request,
                   SdsStat *response) override;

  Status CloseModel(ServerContext *context,
                    const Model *request,
                    SdsStat *response) override;

 private:
  bool CheckUpdate();
  string GetPosTag(const string &slu);
  string GetBestSLU(const string &slu);
  string GetDialogIntent(const string &slu);
  void SetEntities(string &query, Intent *response);
  void ParseOREntities(const string &query, Intent *response);
  void SetEntitesMap(const string key, const string value, Intent *response);
  void SetTlsId(const int64_t session_id, const ServerContext *context);
  void CleanDisabledSession();

  DialogSession *find(const string &key) {
    auto got = sessions_.find(key);
    if (got == sessions_.end())
      return nullptr;
    else {
      auto got2 = session_times_.find(key);
      if (got2 == session_times_.end())
        session_times_.insert(std::make_pair(key, time(nullptr)));
      else
        got2->second = time(nullptr);
      return got->second;
    }
  }

  void insert(const string &key, DialogSession *ses) {
    sessions_.insert(std::make_pair(key, ses));
    session_times_.insert(std::make_pair(key, time(nullptr)));
  }

  DialogSession *remove(const string &key) {
    auto got = sessions_.find(key);
    if (got == sessions_.end())
      return nullptr;
    else {
      DialogSession *ret = got->second;
      sessions_.erase(got);

      auto got2 = session_times_.find(key);
      if (got2 != session_times_.end())
        session_times_.erase(got2);
      // logger_debug("session size = {}", sessions_.size());
      return ret;
    }
  }

 private:
  const std::string &path_;
  const std::string &endpoint_;
  ServerState state_;
  const char *lang_;
  bool initialized_ = false;
  std::recursive_mutex session_lock_;
  std::unordered_map<string, DialogSession *> sessions_;
  std::unordered_map<string, time_t> session_times_;
  long expire_interval_;

  void ResetTlsValues() {
    tls_op_sync_id.clear();
    tls_session_id;
  }
};

void RunSdsChild(const string &group,
                 const string &path,
                 const string &endpoint);

static std::string server_endpoint;

#endif //_SDS_SERVICE_IMPL_H
