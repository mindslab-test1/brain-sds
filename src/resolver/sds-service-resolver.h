#ifndef SDS_SERVICE_RESOLVER_H
#define SDS_SERVICE_RESOLVER_H

#include <grpc++/grpc++.h>
#include <json/json.h>
#include <atomic>

#include "maum/brain/sds/resolver.grpc.pb.h"
#include "maum/brain/sds/model_manager.grpc.pb.h"
#include "maum/brain/sds/sds.grpc.pb.h"
#include "../tls-value.h"

using std::string;
using std::atomic;

using grpc::Status;
using grpc::ServerContext;
using grpc::ServerReader;
using google::protobuf::Empty;
using maum::brain::sds::ModelGroup;
using maum::brain::sds::ModelGroupList;
using maum::brain::sds::ServerStatus;
using maum::brain::sds::ServerStatusList;
using maum::brain::sds::Model;
using maum::brain::sds::ModelList;
using maum::brain::sds::ModelParam;
using maum::brain::sds::ModelSlots;

class SdsServiceResolver {

  SdsServiceResolver();

 public:

  static SdsServiceResolver *GetInstance();
  virtual ~SdsServiceResolver();
  Status Find(ServerContext *context,
              const ModelGroup *request,
              ServerStatus *response);
  Status GetServers(ServerContext *context, ServerStatusList *response);
  Status Stop(ServerContext *context,
              const ModelGroup *request,
              ServerStatus *response);
  Status Restart(ServerContext *context,
                 const ModelGroup *request,
                 ServerStatus *response);
  Status Ping(ServerContext *context,
              const ModelGroup *request,
              ServerStatus *response);
  Status GetAllModels(ServerContext *context,
                      const Empty *empty,
                      ModelList *response);
  Status GetModelGroups(ServerContext *context,
                        const Empty *empty,
                        ModelGroupList *response);
  Status CreateModelGroup(ServerContext *context,
                          const ModelGroup *request,
                          Empty *empty);
  Status DeleteModelGroup(ServerContext *context,
                          const ModelGroup *request,
                          Empty *empty);
  Status UpdateModel(ServerContext *context, const Model *request,
                     ServerReader<::maum::common::FilePart> *reader,
                     Empty *empty);
  Status DeleteModel(ServerContext *context,
                     const Model *request,
                     Empty *empty);
  Status LinkModel(ServerContext *context,
                   const ModelParam *request,
                   Empty *empty);
  Status UnlinkModel(ServerContext *context,
                     const ModelParam *request,
                     Empty *empty);
  Status GetModelSlots(ServerContext *context,
                       const ModelParam *request,
                       ModelSlots *response);
  int ClearModelGroup(pid_t pid);

 private:

  string model_root_;
  string group_root_;
  std::unordered_map<string, ServerStatus> root_;
  //Json::Value root_;
  std::mutex root_lock_;
  std::mutex port_lock_;
  string export_ip_;
  string state_file_;

  enum ForkResult {
    FORK_SUCCESS = 0,
    FORK_NOT_FOUND = 1,
    FORK_FAILED = 2,
    FORK_PORT_ALREADY_USED = 3
  };

  bool IsRunning(const ModelGroup &request, ServerStatus &found);
  bool Stop(const ModelGroup &request, const ServerStatus &item);

//  void WriteStatus();
//  void LoadStatus();
  bool PingServer(ServerStatus &item, bool &updated);
  ForkResult ForkServer(const ModelGroup *model, ServerStatus &item);
  void SetTlsId(const ServerContext *context);
  string GetPath(const bool lang,
                 const bool is_external,
                 const bool is_education);

  // singleton
  static std::mutex mutex_;
  static atomic<SdsServiceResolver *> instance_;
  static const long kWaitTimeoutToStarting = 3;

  void ResetTlsValues() {
    tls_op_sync_id.clear();
  }
};

#endif
