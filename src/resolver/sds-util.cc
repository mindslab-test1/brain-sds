#include <libmaum/common/config.h>
#include <libmaum/common/base64.h>

#include "sds-util.h"

using grpc::ServerContext;
using maum::common::LangCode;
using std::string;

void GetModelFromContext(const ServerContext *ctx, Model &request) {
  const auto &client_map = ctx->client_metadata();
  auto item_l = client_map.find("in.lang");
  if (item_l == client_map.end()) {
    request.set_lang(LangCode::kor);
  } else {
    string lang_name = item_l->second.data();
    LangCode lang;
    if (!maum::common::LangCode_Parse(lang_name, &lang)) {
      LOGGER()->warn("invalid lang {}", lang_name);
      lang = LangCode::kor;
    }
    request.set_lang(lang);
  }

  auto item_e = client_map.find("in.is_external");
  if (item_l == client_map.end()) {
    request.set_is_external(false);
  } else {
    string is_external = item_e->second.data();
    if (strcmp(is_external.c_str(), "true") == 0
        || strcmp(is_external.c_str(), "1") == 0) {
      request.set_is_external(true);
    } else if (strcmp(is_external.c_str(), "false") == 0
        || strcmp(is_external.c_str(), "0") == 0) {
      request.set_is_external(false);
    } else {
      LOGGER()->warn("invalid is_external {}", is_external);
      request.set_is_external(true);
    }
  }

  auto item_m = client_map.find("in.name");
  if (item_m == client_map.end()) {
    LOGGER()->warn("can't find name");
    request.set_name("default"); // TODO: create default model name
  } else {
    request.set_name(item_m->second.data());
  }
}

std::string GetFilenameFromContext(const ServerContext *ctx) {
  const auto &client_map = ctx->client_metadata();
  auto item_s = client_map.find("in.filename");
  if (item_s == client_map.end()) {
    return "";
  } else {
    return Base64Decode(item_s->second.data());
  }
}
