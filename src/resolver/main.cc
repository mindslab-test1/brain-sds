
#include "sds-service-resolver-impl.h"
#include <libmaum/common/config.h>

#include <getopt.h>
#include <csignal>
#include <libmaum/common/util.h>
#include <libmaum/log/call-log.h>
#include <libmaum/rpc/result-status.h>
#include <gitversion/version.h>
#include <dlfcn.h>

using namespace std;
using std::shared_ptr;
using grpc::Server;
using grpc::ServerBuilder;
using libmaum::rpc::CommonResultStatus;

volatile std::sig_atomic_t g_signal_status;

extern void ChildHandler(int sig);

inline void Usage(const char *program) {
  cout << program << " [-h] [-v]" << endl;
  exit(0);
}

inline void HandleSignal(int signal) {
  LOGGER_info("handling {}", signal);
  g_signal_status = signal;
  exit(0);
}

inline void RunServer() {
  libmaum::Config &c = libmaum::Config::Instance();

  auto logger = c.GetLogger();
  std::string server_address(c.Get("maum.sds.listen"));
  long grpc_timeout = c.GetDefaultAsInt("maum.sds.timeout", "0");

  SdsResolverImpl service_resolver;
  ServerBuilder builder;

  if (grpc_timeout > 0) {
    builder.AddChannelArgument(GRPC_ARG_MAX_CONNECTION_IDLE_MS, grpc_timeout);
  }
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  builder.RegisterService(static_cast<maum::brain::sds::SdsServiceResolver::Service *> (&service_resolver));
  builder.RegisterService(static_cast<maum::brain::sds::SdsModelManager::Service *> (&service_resolver));
  unique_ptr<Server> server(builder.BuildAndStart());

  if (server) {
    logger_info("Server listening on {}", server_address);
    c.DumpPid();
    server->Wait();
  }
}

void LoadPlugins() {
  auto &c = libmaum::Config::Instance();
  auto plugin_count = c.GetAsInt("brain.sds.plugin.count");
  for (auto i = 0; i < plugin_count; i++) {
    std::string so_name = c.Get("brain.sds.plugin." + to_string(i + 1) + ".so");
    std::string
        entry = c.Get("brain.sds.plugin." + to_string(i + 1) + ".entrypoint");

    if (so_name.empty() || entry.empty()) {
      LOGGER_warn("invalid so {} or entry {} ", so_name, entry);
      continue;
    }

    void *ptr = dlopen(so_name.c_str(), RTLD_NOW);
    if (ptr == nullptr) {
      LOGGER_warn("cannot load shared object {}", so_name);
      continue;
    }
    void (*sym)();
    sym = reinterpret_cast<void (*)()>(dlsym(ptr, entry.c_str()));
    if (sym == nullptr) {
      LOGGER_warn("cannot load sym {}", entry);
    }
    LOGGER_info("calling {} {}", so_name, entry);
    sym();
  }
}

char *g_program_dir = nullptr;
int main(int argc, char *argv[]) {

  std::signal(SIGINT, HandleSignal);
  //std::signal(SIGSEGV, HandleSignal);
  //std::signal(SIGBUS, HandleSignal);
  std::signal(SIGTERM, HandleSignal);

  auto &c = libmaum::Config::Init(argc, argv, "brain-sds.conf");

  CommonResultStatus::SetModuleAndProcess(maum::rpc::Module::BRAIN_SDS,
                                          maum::rpc::ProcessOfBrainSds::BRAIN_SDS_MAIN);

  bool do_exit = false;

  while (true) {
    int opt_idx = 0;
    static const struct option sds_resolver_option[] = {
        {"version", no_argument, nullptr, 'v'},
        {"help", no_argument, nullptr, 'h'}
    };

    int ch = getopt_long(argc, argv, "vh", sds_resolver_option, &opt_idx);

    if (ch == -1) {
      break;
    }

    switch (ch) {
      default: {
        Usage(argv[0]);
        break;
      }
      case 'v': {
        printf("%s version %s\n", basename(argv[0]), version::VERSION_STRING);
        do_exit = true;
        break;
      }
      case 'h': {
        Usage(argv[0]);
        do_exit = true;
        break;
      }
    }
  }
  if (do_exit)
    exit(EXIT_SUCCESS);

  auto logger = LOGGER();
  logger_debug("argc : {}", argc);

  auto program = c.GetProgramPath();
  auto pos = program.find_last_of('/');
  g_program_dir = strdup(program.substr(0, pos).c_str());

  libmaum::log::CallLogger::GetInstance();

  LoadPlugins();

  signal(SIGCHLD, ChildHandler);
  EnableCoreDump();
  logger_debug("start default server");
  RunServer();
  return 0;
}
