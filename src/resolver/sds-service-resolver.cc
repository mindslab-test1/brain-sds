#include "sds-service-resolver.h"
#include <libmaum/common/config.h>
#include <libmaum/common/util.h>
#include <libmaum/common/fileutils.h>
#include <libmaum/log/call-log.h>
#include <libmaum/rpc/result-status.h>
#include "extract-tar.h"
#include "sds-util.h"
#include "../tls-value.h"
#include <google/protobuf/util/json_util.h>

#include <sys/resource.h>
#include <sys/wait.h>
#include <fstream>

using grpc::StatusCode;
using grpc::ChannelInterface;
using grpc::ServerBuilder;
using grpc::Server;

using maum::common::LangCode;
using libmaum::log::CallLogInvoker;
using libmaum::rpc::CommonResultStatus;

using maum::brain::sds::SpokenDialogService;
using maum::brain::sds::SpokenDialogServiceInternal;
using maum::brain::sds::ServerState;

using namespace std;
//using namespace google::protobuf::util;

// error_index: 00000 ~ 09999
#define ERROR_INDEX_MAJOR 00000

atomic<::SdsServiceResolver *> SdsServiceResolver::instance_;
mutex SdsServiceResolver::mutex_;

SdsServiceResolver *SdsServiceResolver::GetInstance() {
  SdsServiceResolver *tmp = instance_.load(std::memory_order_relaxed);
  std::atomic_thread_fence(std::memory_order_acquire);
  if (tmp == nullptr) {
    std::lock_guard<std::mutex> lock(mutex_);
    tmp = instance_.load(std::memory_order_relaxed);
    if (tmp == nullptr) {
      tmp = new SdsServiceResolver;
      std::atomic_thread_fence(std::memory_order_release);
      instance_.store(tmp, std::memory_order_relaxed);
    }
  }
  return tmp;
}

/**
 * load current and existing state from `run.dir`.
 */
SdsServiceResolver::SdsServiceResolver() {

  // dump current service as json.
  auto &c = libmaum::Config::Instance();
  model_root_ = c.Get("models.dir");
  group_root_ = c.Get("groups.dir");

  char export_ip[128];
  GetPrimaryIp(export_ip, sizeof export_ip);
  export_ip_ = string(export_ip);

  string json_dir = c.Get("run.dir");
  mkpath(json_dir);

  string json_file = c.Get("maum.sds.state.file");
  state_file_ = json_dir + '/' + json_file;

  //LoadStatus();
}

SdsServiceResolver::~SdsServiceResolver() {
  LOGGER_warn("oops! dying");
}

/**
 * 해당 모델 파일이 존재하는지를 점검한다.
 *
 * @param model 모델
 * @param found 발견된 모델 정보 객체
 * @return 모델이 살아 있고 실행 중이면 true를 반환한다.
 */
bool SdsServiceResolver::IsRunning(const ModelGroup &request,
                                   ServerStatus &found) {
  auto logger = LOGGER();
  const string &group_name = request.name();

  lock_guard<mutex> guard(root_lock_);
  auto got = root_.find(group_name);
  guard.~lock_guard();

  if (got == root_.end()) {
    logger_debug("not found {} server", group_name);
    return false;
  } else if (got != root_.end()) {
    found = got->second;
  }

  logger_debug("found {}", found.Utf8DebugString());
  bool updated;
  if (PingServer(found, updated)) {
    if (updated) {
      lock_guard<mutex> guard2(root_lock_);
      got->second = found;
      //WriteStatus();
    }
    return true;
  }
  logger_debug("Remove Member from resolver, {}", group_name);

  lock_guard<mutex> guard2(root_lock_);
  root_.erase(group_name);
  //WriteStatus();
  return false;
}

void SdsServiceResolver::SetTlsId(const ServerContext *context) {
  ResetTlsValues();

  const auto &client_map = context->client_metadata();
  auto item_s = client_map.find("x-operation-sync-id");
  if (item_s != client_map.end()) {
    string xid = string(item_s->second.begin(), item_s->second.end());
    SetTlsOpSyncId(xid);
    LOGGER_info("operation from {}", context->peer());
  } else {
    LOGGER_warn("x-operation-sync-id is not found");
  }
}

string SdsServiceResolver::GetPath(const bool lang,
                                   const bool is_external,
                                   const bool is_education) {
  string path;
  if (is_education) {
    path = "_edu";
  } else {
    if (lang == maum::common::LangCode::kor) {
      path = "_k";
    } else {
      path = "_e";
    }
    if (is_external) {
      path += "e";
    } else {
      path += "i";
    }
  }
  return path;
}
/**
 * 서버는 `fork`로 같은 프로세스를 다시 실행한다.
 *
 * 기존에 실행 중인 서버에 대한 정보를 가져온다. 없으면 새로 실행한다.
 * 실행 후에 `PingServer()`를 호출해서 그 결과를 받아 온다.
 *
 * 이미 서버가 실행 중일 경우에도 PingServer()를 호출해서 동작 여부를 확인한다.
 * 동작이 확인 후 서버가 동작하고 있지 않으면 다시 실행한다.
 * 그래도 정상적이지 않으면 서버 레코드를 삭제한다.
 *
 * @param ctx 호출하는 클라이언트 정보
 * @param query 요청하는 DM 서버 정보
 * @param location 실행중인 서버의 주소
 * @return 서비스 처리 상태, 요청하는 서버가 존재하지 않으면 NOT_FOUND를 반환한다.
 */
Status SdsServiceResolver::Find(ServerContext *context,
                                const ModelGroup *request, ServerStatus *ss) {
  auto logger = LOGGER();
  Status status(Status::OK);
  SetTlsId(context);
  logger_trace("Find request: [{}]", request->Utf8DebugString());

  ServerStatus found;
  bool running = IsRunning(*request, found);

  CallLogInvoker log("Find", context, request, ss, &status);

  if (running) {
    ss->set_server_address(found.server_address());
    ss->set_group_name(request->name());
    ss->set_lang(request->lang());
    ss->set_is_external(request->is_external());
    ss->set_is_education(request->is_education());
    ss->set_running(true);
    ss->set_state(ServerState(found.state()));
    ss->set_invoked_by("find");
    return status;
  }

  ServerStatus item;

  lock_guard<mutex> guard(root_lock_);
  logger_debug("before fork check object exist {}, {}",
               request->name(), request->lang());
  auto got = root_.find(request->name());
  if (got == root_.end()) {
    //item.set_pid(0);
    item.set_group_name(request->name());
    item.set_lang(request->lang());
    item.set_is_external(request->is_external());
    item.set_is_education(request->is_education());
    item.set_server_address(export_ip_ + ':' +
        libmaum::Config::Instance().Get("maum.sds.resolver.port"));
    //item->set_forked_at(0);
    item.set_state(maum::brain::sds::SERVER_STATE_NONE);
    root_.insert(std::make_pair(request->name(), item));
  } else {
    logger_debug("some another call fork for {} {}, now returns",
                 request->name(), request->lang());
    ss->set_server_address(found.server_address());
    ss->set_group_name(request->name());
    ss->set_lang(request->lang());
    ss->set_is_external(request->is_external());
    ss->set_is_education(request->is_education());
    ss->set_running(true);
    ss->set_state(ServerState(found.state()));
    ss->set_invoked_by("find");
    return status;
  }
  guard.~lock_guard();

  ForkResult res = ForkServer(request, item);
  logger_info("[Find] fork res {}", res);
  switch (res) {
    case FORK_SUCCESS: {
      logger_debug("[server endpoint] {}", item.server_address());
      ss->set_server_address(item.server_address());
      ss->set_group_name(request->name());
      ss->set_lang(request->lang());
      ss->set_is_external(request->is_external());
      ss->set_is_education(request->is_education());
      ss->set_running(true);
      ss->set_state(ServerState(item.state()));
      ss->set_invoked_by("find&fork");
      break;
    }
    case FORK_NOT_FOUND: {
      // $MAUM_ROOT/trained/sds-group 경로에 그룹이 존재하지 않을 경우
      logger_warn("cannot find requested path: {} {}",
                  request->name(),
                  request->lang());
      status = ResultGrpcStatus(ExCode::NOT_FOUND,
                                ERROR_INDEX(1001),
                                "Cannot find requested path");
      return status;
    }
    case FORK_FAILED: {
      // child process를 fork 하는 과정에서 실패한 경우
      logger_error("child server fork failed  errno: {}", errno);
      status = ResultGrpcStatus(ExCode::RUNTIME_ERROR,
                                ERROR_INDEX(1002),
                                "Fork failed");
      return status;
    }
    case FORK_PORT_ALREADY_USED: {
      logger_warn("The port you are trying to fork is already in use.");
      return status;
    }
  }

  logger_trace("Find response: [{}]", ss->Utf8DebugString());
  return status;
}

/**
 * 자식 분류기 서버가 죽었을 경우에
 * 이를 정리하도록 처리한다.
 *
 * @param sig 시그널 번호
 */
void ChildHandler(int sig) {
  int status;
  pid_t pid;
  LOGGER_info("sig child called : sig={} pid={}", sig, getpid());
  while ((pid = waitpid(-1, &status, WNOHANG)) > 0) {
    LOGGER_error("CHILD EXIT : {}/{}", pid, status);

    auto resolver = SdsServiceResolver::GetInstance();
    resolver->ClearModelGroup(pid);
  }
}

extern char *g_program_dir;

const char
    *ksub_option_1 = "brain-sds-kor-ext"; // child process name, used full_exe
const char *ksub_option_2 = "brain-sds-eng-ext";
const char *ksub_option_3 = "brain-sds-kor-int";
const char *ksub_option_4 = "brain-sds-eng-int";
const char *ksub_option_5 = "brain-sds-eng-edu";

/**
 * 자식 프로세스를 생성한다.
 *
 * fork가 성공하면 자식 프로그램을 실행한다.
 *
 * @param model 실행할 모델
 * @param item 실행하고 난 후 생성된 item 값
 * @return ForkResult 결과물을 반환한다.
 */
SdsServiceResolver::ForkResult SdsServiceResolver::ForkServer(
    const ModelGroup *request, ServerStatus &item) {

  auto logger = LOGGER();
  libmaum::Config &c = libmaum::Config::Instance();

  const auto &group_name = request->name();
  string full_path = group_root_;
  const int min_port = c.GetAsInt("maum.sds.random.min.port");
  const int max_port = c.GetAsInt("maum.sds.random.max.port");

  full_path += '/';
  full_path += group_name; // ${MAUM_ROOT}/maum/trained/sds-group/group_path

  string path = GetPath(request->lang(),
                        request->is_external(),
                        request->is_education());

  full_path += path;

  int path_found = access(full_path.c_str(), 0);
  if (path_found != 0) {
    logger_debug("{} is not found", full_path);
    return FORK_NOT_FOUND;
  } else {
    logger_debug("{} is found", full_path);
  }

  lock_guard<mutex> guard(port_lock_);
  int port;
  string endpoint;
  unsigned int port_flag = 0;

  for (int cnt = 0;; cnt++) {
    port = GetRandomTcpPort(min_port, max_port);
    endpoint = export_ip_ + ":" + std::to_string(port);

    port_flag = 0;
    for (auto item1:root_) {
      port_flag++;
      if (item1.second.server_address().compare(endpoint) == 0) {
        port_flag = 0;
        break;
      }
    }

    if (port != -1 && port_flag == root_.size()) {
      break;
    }

    if ((cnt > 4) && (port == -1 || port_flag != root_.size())) {
      logger_debug("get port already used");
      return FORK_PORT_ALREADY_USED;
    }
  }
  logger_debug("get port {}", port);

  int pid = fork();

  if (pid == -1) {
    return FORK_FAILED;
  }

  if (pid == 0) {
    rlimit rl = {};
    getrlimit(RLIMIT_NOFILE, &rl);
    for (unsigned int fd = 3; fd < rl.rlim_cur; fd++)
      (void) close(fd);

    char option_d[3] = "-d";
    char option_e[3] = "-e";
    char option_p[3] = "-p";
    char full_exe[PATH_MAX];

    // Check me
    bool is_external = request->is_external();
    string language = maum::common::LangCode_Name(request->lang());
    if (!request->is_education()) {
      if (is_external) {
        if (language == "kor") {
          snprintf(full_exe, sizeof(full_exe), "%s/%s",
                   g_program_dir, ksub_option_1);
        } else {
          snprintf(full_exe, sizeof(full_exe), "%s/%s",
                   g_program_dir, ksub_option_2);
        }
      } else {
        if (language == "kor") {
          snprintf(full_exe, sizeof(full_exe), "%s/%s",
                   g_program_dir, ksub_option_3);
        } else {
          snprintf(full_exe, sizeof(full_exe), "%s/%s",
                   g_program_dir, ksub_option_4);
        }
      }
    } else {
      snprintf(full_exe, sizeof(full_exe), "%s/%s",
               g_program_dir, ksub_option_5);
    }

    char *argv[] = {
        full_exe,
        option_d, (char *) full_path.c_str(),
        option_e, (char *) endpoint.c_str(),
        option_p, (char *) to_string(port).c_str(),
        nullptr};

    // execute
    int r = execv(full_exe, argv);
    if (r == -1) {
      printf("result execv fail [%d]\n", r);
      printf("trace..full_exe %s, option_d %s, full_path %s, option_e %s, "
             "endpoint %s, option_p %s, to_string(port) %s\n",
             full_exe,
             option_d,
             full_path.c_str(),
             option_e,
             endpoint.c_str(),
             option_p,
             to_string(port).c_str());
      logger_error("result execv fail : {}", r);
      logger_error("trace..full_exe {}, option_d {}, full_path {}, "
                   "option_e {}, endpoint {}, option_p {}, to_string(port) {}",
                   full_exe,
                   option_d,
                   full_path.c_str(),
                   option_e,
                   endpoint.c_str(),
                   option_p,
                   to_string(port).c_str());
    }

    // NOT REACH
    return FORK_SUCCESS;
  } else {
    logger_debug("insert forked object {} {}",
                 pid, request->lang());
    logger_debug("[PID] {}", pid);
    logger_debug("[Name] {}", group_name);
    logger_debug("[Lang] {}", request->lang());
    logger_debug("[Is external] {}", request->is_external());
    logger_debug("[END point] {}", endpoint);
    item.set_pid(std::to_string(pid));
    item.set_group_name(group_name);
    item.set_lang(request->lang());
    item.set_is_external(request->is_external());
    item.set_is_education(request->is_education());
    item.set_server_address(endpoint);
    item.set_forked_at(std::to_string(time(nullptr)));
    item.set_state(maum::brain::sds::SERVER_STATE_STARTING);
    {
      lock_guard<mutex> guard(root_lock_);
      auto got = root_.find(group_name);
      if (got != root_.end()) {
        got->second = item;
      } else {
        root_.insert(std::make_pair(group_name, item));
      }
      //WriteStatus();
    }
    return FORK_SUCCESS;
  }
}

/**
 * 현재 메모리에 있는 상태를 모두 DB에 저장한다.
 *
 * DB는 JSON 파일이다.
 * 현재 메모리에 있는 모든 정보를 다 기록한다.
 *
 * 이 함수는 변경이 발생하면 매번 호출되어야 한다.
 *
 * 이 함수는 반드시 LOCK 환경에서만 호출되어야 합니다.
 */
/*void SdsServiceResolver::WriteStatus() {

  std::ofstream ofs(state_file_, std::ios_base::trunc | std::ios_base::out);
  std::string tmp;
  bool first = true;

  google::protobuf::util::JsonPrintOptions json_options;
  json_options.add_whitespace = true;
  json_options.preserve_proto_field_names = true;
  json_options.always_print_primitive_fields = true;

  ofs.clear();
  std::unordered_map<string, ServerStatus>::iterator iter;
  ofs << "{" << std::endl;
  for(iter = root_.begin(); iter != root_.end(); iter++) {
    tmp = "";
    google::protobuf::util::MessageToJsonString(iter->second, &tmp, json_options);
    if (first) {
      ofs << "{\n  \"" << iter->first << "\": " << tmp << "}" << std::endl;
      first = false;
    } else {
      ofs << ",\n{\n  \"" << iter->first << "\": " << tmp << "}" << std::endl;
    }
  }
  ofs << "}" << std::endl;
  //LOGGER_debug("server status json {}", root_);
}*/

/**
 * 기존의 서버 등록 상태를 로딩한다.
 *
 * maum/run/"sds-svcd.dump.json" 파일이 존재하면 파일의 내용을 로딩한다.
 * 하나의 레코드를 로딩하면서 바로바로 서버의 존재여부를 체크한다.
 *
 * ```
 * [
 *    "model-kor":
 *    {
 *      "pid": 1234,
 *      "lang": "kor",
 *      "name": "model",
 *      "path": "model-kor",
 *      "endpoint": "x.x.x.x:nnnn"
 *      "forkedAt" : 1497964026,
 *      "state" : 3
 *    }
 * ]
 * ```
 * 체크할 때에는 `PingServer()` 함수를 호출하도록 한다.
 */
/*void SdsServiceResolver::LoadStatus() {
  std::ifstream ifs(state_file_);
  auto logger = LOGGER();
  if (ifs.fail()) {
    logger_debug("cannot load state json {}: not exist", state_file_);
    return;
  }

  Json::CharReaderBuilder builder;
  builder["collectComments"] = false;

  Json::Value temp;
  string errs;
  bool ok = Json::parseFromStream(builder, ifs, &temp, &errs);
  if (!ok) {
    logger_error("cannot parse state json {} {}", state_file_, errs);
    return;
  }

  logger_debug("loaded status: {}", temp.toStyledString());

  int updated_count = 0;
  for (auto item: temp) {
    bool updated = false;
    auto name = item["group_name"].asString();
    Json::FastWriter fastWriter;
    std::string json_output = fastWriter.write(item);
    ServerStatus json_to_message;
    google::protobuf::util::JsonStringToMessage(json_output, &json_to_message);
    if (!PingServer(json_to_message, updated)) {
      temp[name].clear();
      temp.removeMember(name);
      updated_count++;
    }
    if (updated) {
      temp[name] = item;
      updated_count++;
    }
  }

  lock_guard<mutex> guard(root_lock_);
  //root_ = temp;
  if (updated_count) {
    WriteStatus();
  }
}*/

/**
 * 개별 서버가 동작하고 있는지를 점검한다.
 *
 * 먼저 서버의 PID가 동작중인지 점검한다.
 * 해당 서버로 Ping을 호출해서 그 결과를 확인하고 정상적이면 처리한다.
 * 서버의 상태 정보는 갱신될 수 있다. 서버의 동작 여부에 따라서
 * 갱신이 이뤄지면 이를 반영한다.
 *
 * 서버가 STARTING 상태일 때 직접 서버에 Ping을 호출했을 때
 * 응답이 없을 수도 있는데 시작한 시간 3초 이내에는 정상적인 것으로 판단한다.
 *
 * @param item 개별 서버의 정보
 * @param updated item 정보가 갱산되었는지를 확인한다. 갱신될 정보는 item["state"] 값이다.
 *
 * @return 서버가 동작하고 있으면 true, 서버가 종료된 상태이면 false를 반환한다.
 */
bool SdsServiceResolver::PingServer(ServerStatus &item, bool &updated) {
  auto logger = LOGGER();
  logger_debug("root_.pid: {}", item.pid());
  logger_debug("PingServer item: {}", item.Utf8DebugString());

  if (item.pid().empty()) {
    logger_error("a forking item found : {}", item.group_name());
    return false;
  }

  pid_t pid = std::stoi(item.pid());
  logger_debug("Check pid : {}", pid);
  int rc = kill(pid, 0);

  if (rc < 0) {
    if (errno == ESRCH) {
      logger_error("kill error : {}", errno);
      logger->flush();
      return false;
    }
  }

  string endpoint = item.server_address();

  grpc::ClientContext ctx;
  ModelGroup query;
  query.set_name(item.group_name());

  query.set_lang(item.lang());

  query.set_is_external(item.is_external() != 0);
  query.set_is_education(item.is_education() != 0);

  ServerStatus ss;
  Status status;
  if (query.is_external()) {
    auto sds_stub = SpokenDialogService::NewStub(
        grpc::CreateChannel(endpoint, grpc::InsecureChannelCredentials()));
    status = sds_stub->Ping(&ctx, query, &ss);
  } else {
    auto sds_stub = SpokenDialogServiceInternal::NewStub(
        grpc::CreateChannel(endpoint, grpc::InsecureChannelCredentials()));
    status = sds_stub->Ping(&ctx, query, &ss);
  }
  if (status.ok()) {
    if (item.state() != ss.state()) {
      if (ss.state() == maum::brain::sds::SERVER_STATE_UPDATING) {
        logger_debug("server has been updated at {}. Restart required",
                     item.server_address());
        return false;
      } else {
        logger_debug("update server state as {}", ss.state());
        updated = true;
        item.set_state(ss.state());
        return !ss.server_address().empty();
      }
    } else {
      time_t now = time(nullptr);
      time_t cmp = std::stoi(item.forked_at());
      long diff = abs(now - cmp);
      if (item.state() == maum::brain::sds::SERVER_STATE_STARTING &&
          diff < kWaitTimeoutToStarting) {
        logger_debug("server is still starting: {}, diff: {}",
                     cmp, diff);
        return true;
      } else if (item.state() == maum::brain::sds::SERVER_STATE_INITIALIZING) {
        logger_debug("server is initializing at {}", item.server_address());
        return true;
      } else if (item.state() == maum::brain::sds::SERVER_STATE_RUNNING) {
        logger_debug("server is running at {}", item.server_address());
        return true;
      } else if (item.state() == maum::brain::sds::SERVER_STATE_UPDATING) {
        logger_debug("server has been updated at {}. Restart required",
                     item.server_address());
        return false;
      } else {
        item.set_state(maum::brain::sds::SERVER_STATE_NONE);
        updated = true;
        logger_debug("server not running, update server state as {}",
                     maum::brain::sds::SERVER_STATE_NONE);

        return false;
      }
    }
  } else {
    logger_error("call Ping in child server is fail.");
    logger_debug("status.error_code: [{}]", status.error_code());
    logger_debug("status.error_message: [{}]", status.error_message());
    logger_debug("status.error_details: [{}]", status.error_details());

    time_t now = time(nullptr);
    time_t cmp = std::stoi(item.forked_at());
    long diff = abs(now - cmp);
    if (diff < kWaitTimeoutToStarting) {
      logger_debug("server is still now: {}, starting: {}, diff: {}",
                   now, cmp, diff);
      return true;
    }
  }
  return false;
}
/**
 * 현재의 모델 목록에서 특정한 `pid`를 가진 객체를 삭제한다.
 *
 * SIGCHLD 시그널의 결과를 처리하는 함수이다.
 * @param pid 삭제할 프로세스 ID
 * @return 성공하면 0, 실패하면 -1
 */
int SdsServiceResolver::ClearModelGroup(pid_t pid) {
  lock_guard<mutex> guard(root_lock_);
  for (auto item : root_) {
    if (item.second.pid().empty()) {
      LOGGER_debug("pid {} is empty in root_", pid);
      return -1;
    }
    auto run_pid = std::stoi(item.second.pid());
    if (pid == run_pid) {
      //auto path = item["path"].asString();

      //root_[path].clear();
      //root_.removeMember(path);
      root_.erase(root_.find(item.first));
      //WriteStatus();
      LOGGER_debug("pid {} removed in root_", pid);
      return 0;
    }
  }
  LOGGER_debug("pid {} not found in root_", pid);
  return -1;
}

/**
 * 모든 실행 중인 서버의 목록을 구한다.
 *
 * @param list 모델의 목록
 * @return grpc Status 대부분 OK
 */
Status SdsServiceResolver::GetServers(ServerContext *context,
                                      ServerStatusList *list) {
  SetTlsId(context);
  lock_guard<mutex> guard(root_lock_);
  int sync = 0;
  Status status(Status::OK);

  google::protobuf::Empty empty;
  CallLogInvoker log(__func__, context, &empty, list, &status);

  for (auto item : root_) {
    auto server = list->add_servers();
    server->set_group_name(item.second.group_name());
    server->set_lang(item.second.lang());
    server->set_is_external(item.second.is_external());
    server->set_is_education(item.second.is_education());
    server->set_server_address(item.second.server_address());
    server->set_invoked_by("get servers");
    bool updated = false;
    auto name = item.second.group_name();
    if (PingServer(item.second, updated)) {
      server->set_running(true);
      if (updated) {
        root_.insert(std::make_pair(name, item.second));
        sync++;
      }
    } else {
      lock_guard<mutex> guard2(root_lock_);
      root_.erase(name);
      server->set_running(false);
      sync++;
    }
    server->set_state(ServerState(item.second.state()));
  }
  if (sync)
    //WriteStatus();

    LOGGER_trace("GetServers response: [{}]", list->Utf8DebugString());
  return status;
}

/**
 * 지정한 모델의 서비스를 중지한다.
 *
 * @param model 중지할 모델
 * @param found 발견된 서버 상태 정보
 * @return 성공하면 true, 아니면 false
 */
bool SdsServiceResolver::Stop(const ModelGroup &request,
                              const ServerStatus &found) {
  auto logger = LOGGER();
  logger_debug("found {}", found.Utf8DebugString());
  pid_t pid = std::stoi(found.pid());
  logger_debug("killing pid : {}", pid);

  bool ret = false;
  // TODO
  // 정상적으로 죽도록 처리해야 한다!!
  int rc = kill(pid, SIGTERM);
  if (rc < 0) {
    if (errno == ESRCH) {
      logger_warn("kill ESRCH error : {}", errno);
      ret = true;
    } else if (errno == EPERM) {
      logger_error("kill EPERM error : {}", errno);
      ret = false;
    }
  } else
    ret = true;
  if (ret) {
    const auto &group_name = request.name();
    lock_guard<mutex> guard(root_lock_);
    root_.erase(group_name);
    //WriteStatus();
  }
  return ret;
}

/**
 * 동작 중인 서버를 종료시킨다.
 *
 * @param context 호출 컨텍스트
 * @param m 모델
 * @param server 서버 상태
 * @return grpc &Status::OK
 */
Status SdsServiceResolver::Stop(ServerContext *context, const ModelGroup *m,
                                ServerStatus *server) {
  SetTlsId(context);
  LOGGER_trace("Stop request: [{}]", m->Utf8DebugString());
  Status status(Status::OK);

  CallLogInvoker log(__func__, context, m, server, &status);

  server->set_group_name(m->name());
  server->set_lang(m->lang());
  server->set_invoked_by("stop");

  ServerStatus found;
  auto running = IsRunning(*m, found);
  if (running) {
    Stop(*m, found);
    server->set_running(false);
    server->set_state(maum::brain::sds::SERVER_STATE_NONE);
  } else {
    server->set_running(false);
    server->set_state(maum::brain::sds::SERVER_STATE_NONE);
    server->set_invoked_by("stop&not_running");
  }
  LOGGER_info("server stop success");
  LOGGER_trace("Stop response: [{}]", server->Utf8DebugString());
  return status;
}

/**
 * 동작 중이거나 멈춰있는 모델을 다시 실행한다.
 * 모델의 내용이 바뀌었을 경우에 호출 할 수 있다.
 *
 * @param m 모델
 * @param server 서버의 상태 정보, 실행된 결과를 반환할 데이터
 * @return grpc Status
 */
Status SdsServiceResolver::Restart(ServerContext *context, const ModelGroup *m,
                                   ServerStatus *server) {
  SetTlsId(context);
  LOGGER_trace("Restart request: [{}]", m->Utf8DebugString());

  Status status(Status::OK);
  CallLogInvoker log(__func__, context, m, server, &status);

  server->set_group_name(m->name());
  server->set_lang(m->lang());
  server->set_is_external(m->is_external());
  server->set_is_education(m->is_education());
  server->set_invoked_by("restart");
  ServerStatus found;
  auto running = IsRunning(*m, found);
  if (running) {
    Stop(*m, found);
    server->set_running(false);
  } else {
    server->set_running(false);
    server->set_invoked_by("stop&not_running");
  }

  ServerStatus item;
  ForkResult res = ForkServer(m, item);
  auto logger = LOGGER();
  logger_info("[Restart] fork res {}", res);
  switch (res) {
    case FORK_SUCCESS: {
      logger_debug("[server endpoint] {}", item.server_address());
      server->set_group_name(m->name());
      server->set_lang(m->lang());
      server->set_is_external(m->is_external());
      server->set_is_education(m->is_education());
      server->set_server_address(item.server_address());
      server->set_running(true);
      server->set_state(ServerState(item.state()));
      break;
    }
    case FORK_NOT_FOUND: {
      logger_warn("cannot find path: {} {}", m->name(), m->lang());
      status = ResultGrpcStatus(ExCode::NOT_FOUND,
                                ERROR_INDEX(3001),
                                "Cannot find requested path");
      return status;
    }
    case FORK_FAILED: {
      logger_error("fork failed  errno: {}", errno);
      status = ResultGrpcStatus(ExCode::RUNTIME_ERROR,
                                ERROR_INDEX(3002),
                                "Fork failed");
      return status;
    }
    case FORK_PORT_ALREADY_USED: {
      logger_warn("The port you are trying to fork is already in use.");
      return status;
    }
  }

  LOGGER_trace("Restart response: [{}]", server->Utf8DebugString());
  return status;
}

/**
 * 서버가 실행 중인지 점검한다.
 *
 * @param m 모델
 * @param server 서버 상태 정보, 반환되는 값
 * @return grpc Status
 */
Status SdsServiceResolver::Ping(ServerContext *context,
                                const ModelGroup *m, ServerStatus *server) {
  SetTlsId(context);
  LOGGER_trace("Ping request: [{}]", m->Utf8DebugString());

  Status status(Status::OK);
  CallLogInvoker log(__func__, context, m, server, &status);

  server->set_group_name(m->name());
  server->set_lang(m->lang());
  server->set_is_external(m->is_external());
  server->set_is_education(m->is_education());
  server->set_invoked_by("ping");

  ServerStatus found;
  auto running = IsRunning(*m, found);
  server->set_running(running);
  server->set_state(ServerState(found.state()));
  if (!running) {
    LOGGER_info("{} server is not running and clear server information",
                m->name());
    server->clear_server_address();
    server->set_state(maum::brain::sds::SERVER_STATE_NONE);
  }

  LOGGER_trace("Ping response: [{}]", server->Utf8DebugString());
  return status;
}

/**
 * 모델 경로 설정을 위한 구조체
 */
const struct PathConfig {
  const char *path_suffix;
  maum::common::LangCode lang_code;
  bool external_flag;
  bool education_flag;
} model_paths[] = {
    {"/ke", LangCode::kor, true, false},
    {"/ki", LangCode::kor, false, false},
    {"/ee", LangCode::eng, true, false},
    {"/ei", LangCode::eng, false, false},
    {"/edu", LangCode::eng, false, true}
};

Status SdsServiceResolver::GetAllModels(ServerContext *context,
                                        const Empty *empty,
                                        ModelList *response) {
  SetTlsId(context);
  auto &c = libmaum::Config::Instance();
  auto logger = LOGGER();

  Status status(Status::OK);
  CallLogInvoker log(__func__, context, empty, response, &status);

  string path = c.Get("models.dir");

  for (const auto &pc: model_paths) {
    // 위의 상수 config 에서 가져온다.
    const string dic_path = path + pc.path_suffix;

    DIR *dir;
    dir = opendir(dic_path.c_str());
    if (dir != nullptr) {
      logger_trace("Searching models in '{}'.", dic_path);
      struct dirent *ent = nullptr;
      while ((ent = readdir(dir))) {
        if (strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0) {
          Model *model = response->add_models();
          char *model_name = ent->d_name;
          model->set_name(model_name);
          model->set_lang(pc.lang_code);
          model->set_is_external(pc.external_flag);
          model->set_is_education(pc.education_flag);
        }
      }
      closedir(dir);
    } else {
      logger_trace("Directory '{}' doesn't exist.", dic_path);
    }
  }

  logger_trace("GetAllModels response: [{}]", response->Utf8DebugString());
  return status;
}

Status SdsServiceResolver::GetModelGroups(ServerContext *context,
                                          const Empty *empty,
                                          ModelGroupList *response) {
  SetTlsId(context);
  auto &c = libmaum::Config::Instance();
  auto logger = LOGGER();
  Status status(Status::OK);

  CallLogInvoker log(__func__, context, empty, response, &status);

  string path = c.Get("groups.dir");

  DIR *dir;
  maum::common::LangCode langCode;
  dir = opendir(path.c_str());
  if (dir == nullptr) {
    logger_warn("cannot open directory {}.", path);
    status = ResultGrpcStatus(ExCode::FAILED_PRECONDITION,
                              ERROR_INDEX(6001),
                              "cannot open directory %s", path.c_str());
    return status;
  } else {
    struct dirent *ent = nullptr;
    while ((ent = readdir(dir))) {
      if (strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0) {
        ModelGroup *model_group = response->add_groups();
        char *group_name = ent->d_name;
        logger_debug("found model group name: {}", group_name);

        string sub_group_name = group_name;
        sub_group_name = sub_group_name.substr(strlen(group_name) - 3, 3);
        //char *bin_type = &group_name[strlen(group_name) - 3];
        const char *bin_type = sub_group_name.c_str();
        logger_debug("found model group type: {}", bin_type);

        if (strcmp(bin_type, "edu") == 0) {
          model_group->set_is_education(true);
          model_group->set_is_external(false);
          langCode = maum::common::LangCode::eng;
        } else {
          char *lang = &group_name[strlen(group_name) - 2];

          if (strncmp(lang, "k", 1) == 0) {
            langCode = maum::common::LangCode::kor;
          } else {
            langCode = maum::common::LangCode::eng;
          }

          char *db = &group_name[strlen(group_name) - 1];

          model_group->set_is_external(strncmp(db, "e", 1) == 0);
        }
        model_group->set_name(group_name);
        model_group->set_lang(langCode);
      }
    }
    closedir(dir);
  }

  logger_trace("GetModelGroups response: [{}]", response->Utf8DebugString());
  return status;
}

Status SdsServiceResolver::CreateModelGroup(ServerContext *context,
                                            const ModelGroup *request,
                                            Empty *empty) {
  SetTlsId(context);
  libmaum::Config &c = libmaum::Config::Instance();
  auto logger = LOGGER();
  Status status(Status::OK);

  logger_trace("CreateModelGroup request: [{}]", request->Utf8DebugString());

  CallLogInvoker log(__func__, context, request, empty, &status);

  string model_group = request->name();
  string group_path = c.Get("groups.dir");
  // 미리 '_'를 더하고
  group_path = group_path + '/' + request->name();

  string path = GetPath(request->lang(),
                        request->is_external(),
                        request->is_education());
  group_path += path;

  if (access(group_path.c_str(), F_OK) == 0) {
    logger_warn("model_group already exists. Check {}", group_path);
    status = ResultGrpcStatus(ExCode::ALREADY_EXISTS,
                              ERROR_INDEX(7001),
                              "model_group already exists. Check %s",
                              group_path.c_str());
    return status;
  }

  int mkdir_group = mkdir(group_path.c_str(), ACCESSPERMS);
  if (mkdir_group == 0) {
    logger_info("Successfully created model_group {} directory",
                request->name());
    string dial_path = group_path + "/dialog_domain";
    int mkdir_dial = mkdir(dial_path.c_str(), ACCESSPERMS);
    if (mkdir_dial == 0) {
      logger_info("Successfully added dialog_domain sub-directory");
    } else {
      logger_warn("Failed to make dialog_domain directory");
      status = ResultGrpcStatus(ExCode::FAILED_PRECONDITION,
                                ERROR_INDEX(7002),
                                "Failed to make dialog_domain directory");
      return status;
    }
  } else {
    logger_warn("Failed to make model_group {} directory", request->name());
    status = ResultGrpcStatus(ExCode::FAILED_PRECONDITION,
                              ERROR_INDEX(7002),
                              "Failed to make model_group %s directory",
                              request->name().c_str());
    return status;
  }
  return status;
}

Status SdsServiceResolver::DeleteModelGroup(ServerContext *context,
                                            const ModelGroup *request,
                                            Empty *empty) {
  SetTlsId(context);
  libmaum::Config &c = libmaum::Config::Instance();
  auto logger = LOGGER();
  Status status(Status::OK);

  logger_trace("DeleteModelGroup request: [{}]", request->Utf8DebugString());

  CallLogInvoker log(__func__, context, request, empty, &status);

  string model_group = request->name();
  string path = c.Get("groups.dir");
  path += "/" + model_group;

  string flag_path = GetPath(request->lang(),
                             request->is_external(),
                             request->is_education());
  path += flag_path;

  int is_dir = access(path.c_str(), 0);

  //TODO: change system() to more stable function

  if (is_dir == 0) {  //directory가 존재하느냐
    string message = "setsid rm -rf " + path;

    if (system(message.c_str()) == 0) {
      logger_debug("command {} successfully executed", message);
    } else {
      logger_debug("command {} executed", message);
      logger_warn("Failed remove {} in sds-group directory", model_group);
      status = ResultGrpcStatus(ExCode::FAILED_PRECONDITION,
                                ERROR_INDEX(8001),
                                "Failed remove %s in sds-group directory",
                                model_group.c_str());
      return status;
    }
  } else {
    logger_debug("Not exist {} directory", model_group);
  }

  return status;
}

Status SdsServiceResolver::UpdateModel(ServerContext *context,
                                       const Model *request,
                                       ServerReader<::maum::common::FilePart> *reader,
                                       Empty *empty) {
  SetTlsId(context);
  libmaum::Config &c = libmaum::Config::Instance();
  auto logger = LOGGER();
  Status status(Status::OK);

  logger_trace("UpdateModel request: [{}]", request->Utf8DebugString());

  CallLogInvoker log(__func__, context, request, empty, &status);

  string resource_path;
  string lang;
  string path = model_root_;
  string model_name = request->name();

  if (request->is_education()) {
    logger_debug("resource is linked as: edu");
    resource_path = c.Get("resources.edu.dir");
    path += "/edu/";
  } else {
    if (request->lang() == maum::common::LangCode::kor) {
      logger_debug("resource is linked as: kor");
      resource_path = c.Get("resources.kor.dir");
      lang = "k";
    } else {
      logger_debug("resource is linked as: eng");
      resource_path = c.Get("resources.eng.dir");
      lang = "e";
    }
    path += "/" + lang;

    if (request->is_external()) {
      path += "e/";
    } else {
      path += "i/";
    }
  }

  if (!ExtractTar(reader, path)) {
    logger_debug("extract tar file");
    logger_warn("cannot decompress tar to {}", path);
    status = ResultGrpcStatus(ExCode::FAILED_PRECONDITION,
                              ERROR_INDEX(9001),
                              "cannot decompress tar to %s", path.c_str());
    return status;
  } else {
    logger_debug("extract tar file successfully executed");
  }

  // internal model은 external model로서도 활용 가능하기 때문에 symbolic link를 걸도록 한다
  // ke, ki, ee, ei 구조에서만 동작하도록 한다.
  if ((!request->is_external()) && (!request->is_education())) {
    string target = model_root_ + "/" + lang + "e/" + model_name;
    string model = model_root_ + "/" + lang + "i/" + model_name;

    if (access(target.c_str(), F_OK) == 0) {
      logger_warn("model {} already exists in {}", model_name, target);
      return status; // TODO: fix me
    } else if (access(model.c_str(), F_OK | R_OK | W_OK) != 0) {
      logger_warn("cannot find model {} in directory {}", model_name, model);
    } else {
      logger_debug("model {} will be added", model_name);
      int sym_link = symlink(model.c_str(), target.c_str());

      if (sym_link < 0) {
        logger_warn("UpdateModel: cannot export internal model {} to external",
                    errno, model_name);
      } else {
        logger_debug("UpdateModel: internal model {} is exported to external",
                     model_name);
      }
    }
  } else {
    logger_debug("this model is external model");
  }

  return status;
}

Status SdsServiceResolver::DeleteModel(ServerContext *context,
                                       const Model *request, Empty *empty) {
  SetTlsId(context);
  libmaum::Config &c = libmaum::Config::Instance();
  auto logger = LOGGER();
  Status status(Status::OK);

  logger_trace("DeleteModel request: [{}]", request->Utf8DebugString());

  CallLogInvoker log(__func__, context, request, empty, &status);

  string model = request->name();
  string path = c.Get("models.dir");
  string resource;
  char lang;

  if (request->is_education()) {
    path += "/edu/";
  } else {
    if (request->lang() == maum::common::LangCode::kor) {
      logger_debug("resource is linked as: kor");
      resource = c.Get("resources.kor.dir");
      path += "/k";
      lang = 'k';
    } else {
      logger_debug("resource is linked as: eng");
      resource = c.Get("resources.eng.dir");
      path += "/e";
      lang = 'e';
    }

    if (request->is_external()) {
      path += "e/";
    } else {
      path += "i/";

      string target = path;
      target = model_root_ + "/" + lang + "e/" + model;
      logger_debug("target: {}", target);

      if (access(target.c_str(), F_OK) != 0) {
        logger_warn("model {} does not exist", model);
        return status; // TODO: fix me
      } else { // internal 모델이 external 그룹에서 사용되고 있을때, 링크를 끊는다.
        logger_debug("model {} will be removed", model);
        struct stat st = {};
        lstat(target.c_str(), &st);
        if (S_ISLNK(st.st_mode)) {
          int result = unlink(target.c_str());
          if (result < 0) {
            logger_warn("UnlinkModel: cannot unlink {} errno: {}",
                        target,
                        errno);
          } else {
            logger_debug("UnlinkModel: model is successfully removed");
          }
        } else {
          logger_warn(
              "UnlinkModel: Check directory {} - not a symbolic link. ",
              target);
        }
      }
    }
  }
  path += "/" + model;

  int is_dir = access(path.c_str(), 0);

  logger_debug("0");
  if (is_dir == 0) {  //directory가 존재하느냐
    RemoveFile(path.c_str(), FILEUTILS_RECUR | FILEUTILS_FORCE); // 디렉토리 삭제
    logger_debug("delete success {} directory", path);
  } else {
    logger_debug("Not exist {} directory", model);
  }

  return status;
}

Status SdsServiceResolver::LinkModel(ServerContext *context,
                                     const ModelParam *request,
                                     Empty *empty) {
  SetTlsId(context);
  auto &c = libmaum::Config::Instance();
  auto logger = LOGGER();
  Status status(Status::OK);

  logger_trace("LinkModel request: [{}]", request->Utf8DebugString());

  CallLogInvoker log(__func__, context, request, empty, &status);

  string model_name = request->model_name();
  string group_name = request->group_name();
  string target = c.Get("groups.dir");
  string model = c.Get("models.dir");

  if (request->is_education()) {
    model += "/edu/";
    group_name += "_edu";
  } else {
    if (request->lang() == maum::common::LangCode::kor) {
      model += "/k";
      group_name += "_k";
    } else {
      model += "/e";
      group_name += "_e";
    }
    if (request->is_external()) {
      model += "e/";
      group_name += "e";
    } else {
      model += "i/";
      group_name += "i";
    }
  }

  target = target + "/" + group_name + "/dialog_domain/" + model_name;
  model += model_name;

  if (access(target.c_str(), F_OK) == 0) {
    logger_warn("model {} already exists in {}", model_name, target);
    return status; // TODO: fix me
  } else if (access(model.c_str(), F_OK | R_OK | W_OK) != 0) {
    logger_warn("cannot find model {} in directory {}", model_name, model);
  } else {
    logger_debug("model {} will be added", model_name);
    int sym_link = symlink(model.c_str(), target.c_str());
    if (sym_link < 0) {
      logger_warn("LinkModel: cannot symlink {} from: {} to: {}",
                  errno, model, target);
    } else {
      logger_debug("LinkModel: model is successfully added");
    }
  }
  return status;
}

Status SdsServiceResolver::UnlinkModel(ServerContext *context,
                                       const ModelParam *request,
                                       Empty *empty) {
  SetTlsId(context);
  auto logger = LOGGER();
  auto &c = libmaum::Config::Instance();
  Status status(Status::OK);

  logger_trace("UnlinkModel request: [{}]", request->Utf8DebugString());

  CallLogInvoker log(__func__, context, request, empty, &status);

  string model_name = request->model_name();
  string group_name = request->group_name();
  string target = c.Get("groups.dir");

  string path = GetPath(request->lang(),
                        request->is_external(),
                        request->is_education());

  group_name += path;

  target = target + "/" + group_name + "/dialog_domain/" + model_name;

  if (access(target.c_str(), F_OK) != 0) {
    logger_warn("model {} does not exist", model_name);
    return status; // TODO: fix me
  } else {
    logger_debug("model {} will be removed", model_name);
    struct stat st = {};
    lstat(target.c_str(), &st);
    if (S_ISLNK(st.st_mode)) {
      int result = unlink(target.c_str());
      if (result < 0) {
        logger_warn("UnlinkModel: cannot unlink {} errno: {}", target, errno);
      } else {
        logger_debug("UnlinkModel: model is successfully removed");
      }
    } else {
      logger_warn("UnlinkModel: Check directory {} - not a symbolic link. ",
                  target);
    }
  }
  return status;
}

Status SdsServiceResolver::GetModelSlots(ServerContext *context,
                                         const ModelParam *request,
                                         ModelSlots *response) {
  SetTlsId(context);
  auto logger = LOGGER();
  auto &c = libmaum::Config::Instance();

  logger_trace("GetModelslots request: [{}]", request->Utf8DebugString());

  string model_name = request->model_name();
  string group_name = request->group_name();
  string target = c.Get("groups.dir");

  string ext = GetPath(request->lang(),
                       request->is_external(),
                       request->is_education());

  group_name += ext;

  target += '/';
  target += group_name;
  target += "/dialog_domain/";
  target += model_name;

  string path(target);
  path += '/';
  path += model_name;
  path += ".slot.txt";

  logger_trace("=== GetPreReqSlots : ", path.c_str());

  string line;
  std::ifstream slot_file(path.c_str());
  bool slot_elem = false;
  while (getline(slot_file, line)) {
    //logger_debug(" --- line : {}", line);
    if (strcmp("</slots>", line.c_str()) == 0) {
      slot_elem = false;
    }
    if (slot_elem) {
      response->add_slot_names(line);
    }
    if (strcmp("<slots>", line.c_str()) == 0) {
      slot_elem = true;
    }
  }
  slot_file.close();

  response->set_model_name(model_name);
  response->set_group_name(group_name);
  response->set_lang(request->lang());

  logger_trace("GetModelslots response: [{}]", response->Utf8DebugString());
  return Status::OK;
}
