#ifndef SDS_MODEL_RESOLVER_IMPL_H
#define SDS_MODEL_RESOLVER_IMPL_H

#include <grpc++/grpc++.h>

#include "maum/brain/sds/resolver.grpc.pb.h"
#include "maum/brain/sds/model_manager.grpc.pb.h"
#include "maum/brain/sds/sds.grpc.pb.h"
#include "sds-service-resolver.h"
#include "sds-util.h"
#include "extract-tar.h"
#include "../tls-value.h"

using SdsRes = maum::brain::sds::SdsServiceResolver;
using SdsMgr = maum::brain::sds::SdsModelManager;

class SdsResolverImpl final : public SdsRes::Service, public SdsMgr::Service {

 public:

  SdsResolverImpl() {
    resolver_ = SdsServiceResolver::GetInstance();
  }

  ~SdsResolverImpl() final = default;

  const char *Name() {
    return "SDS Service Resolver";
  }

  Status Find(ServerContext *context,
              const ModelGroup *request,
              ServerStatus *status) override {
    return resolver_->Find(context, request, status);
  }
  Status GetServers(ServerContext *context,
                    const Empty *empty,
                    ServerStatusList *list) override {
    return resolver_->GetServers(context, list);
  }
  Status Stop(ServerContext *context,
              const ModelGroup *request,
              ServerStatus *status) override {
    return resolver_->Stop(context, request, status);
  }
  Status Restart(ServerContext *context,
                 const ModelGroup *request,
                 ServerStatus *status) override {
    return resolver_->Restart(context, request, status);
  }
  Status Ping(ServerContext *context,
              const ModelGroup *request,
              ServerStatus *status) override {
    return resolver_->Ping(context, request, status);
  }
  Status GetAllModels(ServerContext *context,
                      const Empty *empty,
                      ModelList *response) override {
    return resolver_->GetAllModels(context, empty, response);
  }
  Status GetModelGroups(ServerContext *context,
                        const Empty *empty,
                        ModelGroupList *response) override {
    return resolver_->GetModelGroups(context, empty, response);
  }
  Status CreateModelGroup(ServerContext *context,
                          const ModelGroup *request,
                          Empty *empty) override {
    return resolver_->CreateModelGroup(context, request, empty);
  }
  Status DeleteModelGroup(ServerContext *context,
                          const ModelGroup *request,
                          Empty *empty) override {
    return resolver_->DeleteModelGroup(context, request, empty);
  }
  Status UpdateModel(ServerContext *context,
                     ServerReader<::maum::common::FilePart> *reader,
                     Empty *empty) override {
    Model model;
    GetModelFromContext(context, model);
    Status st = resolver_->UpdateModel(context, &model, reader, empty);
    return st;
  }
  Status DeleteModel(ServerContext *context,
                     const Model *request,
                     Empty *empty) override {
    return resolver_->DeleteModel(context, request, empty);
  }
  Status LinkModel(ServerContext *context,
                   const ModelParam *request,
                   Empty *empty) override {
    return resolver_->LinkModel(context, request, empty);
  }
  Status UnlinkModel(ServerContext *context,
                     const ModelParam *request,
                     Empty *empty) override {
    return resolver_->UnlinkModel(context, request, empty);
  }
  Status GetModelSlots(ServerContext *context,
                       const ModelParam *request,
                       ModelSlots *res) override {
    return resolver_->GetModelSlots(context, request, res);

  }

 private:
  SdsServiceResolver *resolver_;

};

#endif
