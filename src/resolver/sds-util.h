#ifndef __SDS_UTIL_H
#define __SDS_UTIL_H

#include <grpc++/grpc++.h>
#include <maum/brain/sds/resolver.pb.h>

using grpc::ServerContext;
using maum::brain::sds::Model;

void GetModelFromContext(const ServerContext *ctx, Model &request);
std::string GetFilenameFromContext(const ServerContext *ctx);

/*
inline std::string GetModelGroupPath(const ModelGroup &request) {
  std::string model_name = request.name();
  //model_path += '-';
  //model_path += maum::common::LangCode_Name(model.lang());
  return model_path;
}
*/
#endif // __SDS_UTIL_H
