#ifndef _EXTRACT_TAR_H
#define _EXTRACT_TAR_H

#include <grpc++/grpc++.h>
#include <maum/brain/sds/model_manager.grpc.pb.h>

using grpc::ServerReader;
using maum::common::FilePart;

bool ExtractTar(ServerReader<FilePart> *reader,
                const string &to_dir);

#endif //_EXTRACT_TAR_H
