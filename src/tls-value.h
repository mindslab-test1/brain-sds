#ifndef PROJECT_TLS_VALUE_H
#define PROJECT_TLS_VALUE_H

#include <string>

using std::string;

extern thread_local string tls_op_sync_id;
extern thread_local int64_t tls_session_id;

#define log_common(level, format, ...) \
logger->level("{} {} " format, tls_op_sync_id, tls_session_id, ##__VA_ARGS__)
#define LOG_common(level, format, ...) \
LOGGER()->level("{} {} " format, tls_op_sync_id, tls_session_id, ##__VA_ARGS__)

#define logger_trace(format, ...) log_common(trace, format, ##__VA_ARGS__)
#define logger_debug(format, ...) log_common(debug, format, ##__VA_ARGS__)
#define logger_info(format, ...) log_common(info, format, ##__VA_ARGS__)
#define logger_warn(format, ...) log_common(warn, format, ##__VA_ARGS__)
#define logger_error(format, ...) log_common(error, format, ##__VA_ARGS__)
#define logger_critical(format, ...) log_common(critical, format, ##__VA_ARGS__)
#define logger_flush() logger->flush()

#define LOGGER_trace(format, ...) LOG_common(trace, format, ##__VA_ARGS__)
#define LOGGER_debug(format, ...) LOG_common(debug, format, ##__VA_ARGS__)
#define LOGGER_info(format, ...) LOG_common(info, format, ##__VA_ARGS__)
#define LOGGER_warn(format, ...) LOG_common(warn, format, ##__VA_ARGS__)
#define LOGGER_error(format, ...) LOG_common(error, format, ##__VA_ARGS__)
#define LOGGER_critical(format, ...) LOG_common(critical, format, ##__VA_ARGS__)
#define LOGGER_flush() LOGGER()->flush()

inline void SetTlsOpSyncId(const string &id) {
  tls_op_sync_id = id;
}

inline void SetTlsSessionId(const int64_t id) {
  tls_session_id = id;
}

#endif //PROJECT_TLS_VALUE_H
